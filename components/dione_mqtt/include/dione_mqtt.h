#ifndef _DIONE_MQTT_H_
#define _DIONE_MQTT_H_
#include <stdint.h>
#include <string.h>

#include "dione_mqtt_msg.h"
#include "dione_ringbuf.h"

typedef void (* dione_mqtt_callback)(void *, void *);

typedef struct {
    dione_mqtt_callback connected_cb;
    dione_mqtt_callback disconnected_cb;
    dione_mqtt_callback reconnect_cb;

    dione_mqtt_callback subscribe_cb;
    dione_mqtt_callback publish_cb;
    dione_mqtt_callback data_cb;

    char host[CONFIG_MQTT_MAX_HOST_LEN];
    uint32_t port;
    char client_id[CONFIG_MQTT_MAX_CLIENT_LEN];
    char username[CONFIG_MQTT_MAX_USERNAME_LEN];
    char password[CONFIG_MQTT_MAX_PASSWORD_LEN];
    char lwt_topic[CONFIG_MQTT_MAX_LWT_TOPIC];
    char lwt_msg[CONFIG_MQTT_MAX_LWT_MSG];
    uint32_t lwt_qos;
    uint32_t lwt_retain;
    uint32_t clean_session;
    uint32_t keepalive;
} dione_mqtt_settings;

typedef struct dione_mqtt_event_data_t
{
  uint8_t type;
  const char* topic;
  const char* data;
  uint16_t topic_length;
  uint16_t data_length;
  uint16_t data_offset;
  uint16_t data_total_length;
} dione_mqtt_event_data_t;

typedef struct dione_mqtt_state_t
{
  uint16_t port;
  int auto_reconnect;
  dione_mqtt_connect_info_t* connect_info;
  uint8_t* in_buffer;
  uint8_t* out_buffer;
  int in_buffer_length;
  int out_buffer_length;
  uint16_t message_length;
  uint16_t message_length_read;
  dione_mqtt_message_t* outbound_message;
  dione_mqtt_connection_t mqtt_connection;
  uint16_t pending_msg_id;
  int pending_msg_type;
  int pending_publish_qos;
} dione_mqtt_state_t;

typedef struct  {
  int socket;

#if defined(CONFIG_MQTT_SECURITY_ON)  // ENABLE MQTT OVER SSL
  SSL_CTX *ctx;
  SSL *ssl;
#endif

  dione_mqtt_settings *settings;
  dione_mqtt_state_t  mqtt_state;
  dione_mqtt_connect_info_t connect_info;
  QueueHandle_t xSendingQueue;
  DIONE_RINGBUF send_rb;
  uint32_t keepalive_tick;
} dione_mqtt_client;

dione_mqtt_client *dione_mqtt_start(dione_mqtt_settings *mqtt_info);
void dione_mqtt_stop();
//void mqtt_task(void *pvParameters);
void dione_mqtt_subscribe(dione_mqtt_client *client, char *topic, uint8_t qos);
void dione_mqtt_publish(dione_mqtt_client* client, char *topic, char *data, int len, int qos, int retain);
//void mqtt_detroy();
#endif
