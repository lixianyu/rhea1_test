#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := Rhea

#EXTRA_COMPONENT_DIRS := $(PROJECT_PATH)/components/mac_lora/region
include $(IDF_PATH)/make/project.mk
#COMPONENT_DIRS += $(PROJECT_PATH)/components/mac_lora/region
#COMPONENT_ADD_INCLUDEDIRS := components/include
CFLAGS += -Wno-error=char-subscripts \
					-Wno-unused-function \
					-DREGION_CN470
CPPFLAGS += -Wno-unused-function
#-Wno-error=unused-function \
#CPPFLAGS += -DMQTT_SERVER_DIONE -DLXY
#CPPFLAGS += -Werror=char-subscripts
