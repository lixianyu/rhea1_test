#ifndef _DIONE_EVENT_H_
#define _DIONE_EVENT_H_

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

/* The event group allows multiple bits for each event */
#define DIONE_EVENT_STA_GOT_IP BIT0
#define DIONE_EVENT_MQTT_CONNECTED BIT1
#define DIONE_EVENT_MQTT_CONNECTED2 BIT5
#define DIONE_EVENT_UWB_START BIT2
#define DIONE_EVENT_OTA_START BIT3
#define DIONE_EVENT_ADV_SCAN  BIT4
#define UDP_CONNCETED_SUCCESS BIT6
#define LORA_GW_STATE_RXDONE BIT7
#define LED_TOGGLE BIT8
extern EventGroupHandle_t dione_event_group;

#endif
