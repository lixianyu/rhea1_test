#include <time.h>
#include <sys/time.h>

#include "Dione_fun.h"
#include "dione_config.h"
#include "esp_log.h"
#include "driver/i2c.h"
#include "Dione_event.h"
#include "mqtt.h"
#include "dione_mqtt.h"

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

extern esp_err_t dione_nvs_save_seq(void);
extern char gCID[16];
extern uint64_t gSeq;
extern mqtt_client *g_mqtt_client;
#ifdef SECOND_MQTT_SERVER
extern dione_mqtt_client *g_mqtt_client2;
#endif
static const char *TAG_DIONE_FUN = "Dione_Fun";
#ifdef MQTT_SERVER_SH_HUOWEI
static float gTemperature;
static float gHumidity;
#endif

#define SHT21_CMD_TEMP_T_H         0xE3 // command trig. temp meas. hold master
#define SHT21_CMD_HUMI_T_H         0xE5 // command trig. humidity meas. hold master
#define SHT21_CMD_TEMP_T_NH        0xF3 // command trig. temp meas. no hold master
#define SHT21_CMD_HUMI_T_NH        0xF5 // command trig. humidity meas. no hold master
#define SHT21_CMD_WRITE_U_R        0xE6 // command write user register
#define SHT21_CMD_READ_U_R         0xE7 // command read user register
#define SHT21_CMD_SOFT_RST         0xFE // command soft reset

#define SHT21_I2C_ADDRESS      0x40
#define TEMP_RES 16384
#define HUMI_RES 4096
#define I2C_MASTER_NUM I2C_NUM_1
#define WRITE_BIT  I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT   I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN   0x1     /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS  0x0     /*!< I2C master will not check ack from slave */
#define ACK_VAL    0x0         /*!< I2C ack value */
#define NACK_VAL   0x1         /*!< I2C nack value */

static void dione_i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = DIONE_I2C_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = DIONE_I2C_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = 100000;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       0,
                       0, 0);
}

#ifdef MQTT_SERVER_SH_HUOWEI
static void dione_read_SHT21_temperature(void)
{
    uint8_t data_h, data_l;
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, SHT21_CMD_TEMP_T_NH, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    int ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ret;
    }
#endif
    vTaskDelay(100 / portTICK_RATE_MS);

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ESP_FAIL;
    }
#endif
    uint16_t ST = ((data_h << 8) | data_l) >> 2;
    ESP_LOGE(TAG_DIONE_FUN, "ST = 0x%x", ST);
    gTemperature = (ST * 175.72 / TEMP_RES) - 46.85;
}

static void dione_read_SHT21_humidity(void)
{
    uint8_t data_h, data_l;
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, SHT21_CMD_HUMI_T_NH, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    int ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ret;
    }
#endif
    vTaskDelay(35 / portTICK_RATE_MS);

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ESP_FAIL;
    }
#endif
    uint16_t SRH = ((data_h << 8) | data_l) >> 4;
    ESP_LOGE(TAG_DIONE_FUN, "SRH = 0x%x", SRH);
    gHumidity = (SRH * 125 / HUMI_RES) - 6;
}

void dione_temp_task(void *pvParameters)
{
    ESP_LOGD(TAG_DIONE_FUN, "Enter %s", __func__);
#ifdef SECOND_MQTT_SERVER
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED2,
                        false, false, portMAX_DELAY);
#else
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
#endif
    dione_i2c_master_init();
    vTaskDelay(4000 / portTICK_RATE_MS);
    while (true)
    {
        dione_read_SHT21_temperature();
        vTaskDelay(1000 / portTICK_RATE_MS);
        dione_read_SHT21_humidity();

        char *json_buf = (char*)malloc(1024);
        sprintf(json_buf, "{\"cid\":\"%s\",\"seq\":%llu,\"cmd\":%d,\"time\":%ld,\"msg\":{\"Temperature\":%.2f,\"Humidity\":%.1f}}", 
            gCID, gSeq++, MQTT_UP_MSG_TYPE_0, time(NULL), gTemperature, gHumidity);
        #if 0
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)json_buf, strlen(json_buf), 0, 0);
        #else
        mqtt_publish(g_mqtt_client, "topic02", (char*)json_buf, strlen(json_buf), 0, 0);
        #endif
        vTaskDelay(1001 / portTICK_RATE_MS);
        #if 0
        sprintf(json_buf, "{\"Temperature\":%.2f,\"Humidity\":%.1f}", gTemperature, gHumidity);
        mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)json_buf, strlen(json_buf), 0, 0);
        #endif
        free(json_buf);
        dione_nvs_save_seq();
        vTaskDelay(30000 / portTICK_RATE_MS);
    }
}
#else
static void dione_read_SHT21_temperature(void)
{
    uint8_t data_h, data_l;
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, SHT21_CMD_TEMP_T_NH, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    int ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ret;
    }
#endif
    vTaskDelay(100 / portTICK_RATE_MS);

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ESP_FAIL;
    }
#endif
    uint16_t ST = ((data_h << 8) | data_l) >> 2;
    ESP_LOGE(TAG_DIONE_FUN, "ST = 0x%x", ST);
    float temp = (ST * 175.72 / TEMP_RES) - 46.85;
    char *buf = malloc(512);
    memset(buf, 0, 512);
#ifdef SECOND_MQTT_SERVER
    sprintf(buf, "{\"Temperature\":%.2f}", temp);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf);
    mqtt_publish(g_mqtt_client2, "dp/5936377a4eb6400001eb131a", (char*)buf, strlen(buf), 0, 0);
#else
#ifdef MQTT_SERVER_DIONE
    sprintf(buf, "%.2f", temp);
    ESP_LOGW(TAG_DIONE_FUN, "Current Temperature : %.2f", temp);
    mqtt_publish(g_mqtt_client, "Dione/Temperature", (char*)buf, strlen(buf), 0, 0);
#elif defined(MQTT_SERVER_ONENET)
    buf[0] = 3;
    buf[1] = 0x00;
    sprintf(buf+3, "{\"temperature\":%.1f}", temp);
    buf[2] = strlen(buf+3);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf+3);
    mqtt_publish(g_mqtt_client, "$dp", (char*)buf, buf[2] + 3, 0, 0);
#elif defined(MQTT_SERVER_MICRODUINO)
    sprintf(buf, "{\"Temperature\":%.2f}", temp);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf);
    mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)buf, strlen(buf), 0, 0);
#endif
#endif
    free(buf);
}

static void dione_read_SHT21_humidity(void)
{
    uint8_t data_h, data_l;
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, SHT21_CMD_HUMI_T_NH, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    int ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ret;
    }
#endif
    vTaskDelay(35 / portTICK_RATE_MS);

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SHT21_I2C_ADDRESS << 1 | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
#if 0
    if (ret == ESP_FAIL) {
        return ESP_FAIL;
    }
#endif
    uint16_t SRH = ((data_h << 8) | data_l) >> 4;
    ESP_LOGE(TAG_DIONE_FUN, "SRH = 0x%x", SRH);
    float humidity = (SRH * 125 / HUMI_RES) - 6;
    char *buf = malloc(512);
    memset(buf, 0, 512);
#ifdef SECOND_MQTT_SERVER
    sprintf(buf, "{\"Humidity\":%.1f}", humidity);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf);
    mqtt_publish(g_mqtt_client2, "dp/5936377a4eb6400001eb131a", (char*)buf, strlen(buf), 0, 0);
#else
#ifdef MQTT_SERVER_DIONE
    ESP_LOGW(TAG_DIONE_FUN, "Current Humidity : %.2f%%", humidity);
    sprintf(buf, "%.0f%%", humidity);
    mqtt_publish(g_mqtt_client, "Dione/Humidity", (char*)buf, strlen(buf), 0, 0);
#elif defined(MQTT_SERVER_ONENET)
    buf[0] = 3;
    buf[1] = 0x00;
    sprintf(buf+3, "{\"humidity\":%.0f}", humidity);
    buf[2] = strlen(buf+3);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf+3);
    mqtt_publish(g_mqtt_client, "$dp", (char*)buf, buf[2] + 3, 0, 0);
#elif defined(MQTT_SERVER_MICRODUINO)
    sprintf(buf, "{\"Humidity\":%.1f}", humidity);
    ESP_LOGW(TAG_DIONE_FUN, "%s", buf);
    mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)buf, strlen(buf), 0, 0);
#endif
#endif
    free(buf);
}

void dione_temp_task(void *pvParameters)
{
    ESP_LOGD(TAG_DIONE_FUN, "Enter %s", __func__);
#ifdef SECOND_MQTT_SERVER
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED2,
                        false, false, portMAX_DELAY);
#else
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
#endif
    dione_i2c_master_init();
    vTaskDelay(4000 / portTICK_RATE_MS);
    while (true)
    {
        dione_read_SHT21_temperature();
        vTaskDelay(1000 / portTICK_RATE_MS);
        dione_read_SHT21_humidity();
        vTaskDelay(10000 / portTICK_RATE_MS);
    }
}
#endif

