/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: LoRaMac classA device implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/

/*! \file classA/SensorNode/main.c */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/queue.h"
#include "esp_log.h"
//#include "board.h"
#include "sx1276-board.h"
#include "LoRaMac.h"
#include "Region.h"
#include "Commissioning.h"
#include "Dione_config.h"

extern void dumpBytes(const uint8_t *data, size_t count);
extern uint64_t system_get_rtc_time(void);

static const char *TAG_NODE = "Rhea_node";

//#define REGION_CN470
static xQueueHandle g_lora_mac_queue;

// See https://mydevices.com/cayenne/docs/lora/
enum {
    LPP_DIGITAL_INPUT = 0x00, // 1 byte
    LPP_DIGITAL_OUTPUT = 0x01, // 1 byte
    LPP_ANALOG_INPUT = 0x02, // 2 byte
    LPP_ANALOG_OUTPUT = 0x03, // 2 byte
    LPP_ILLUMINANCE_SENSOR = 0x65, // 2 byte
    LPP_PRESENCE_SENSOR = 0x66, // 1 byte
    LPP_TEMPERATURE_SENSOR = 0x67, // 2 byte
    LPP_HUMIDITY_SENSOR = 0x68, // 1 byte
    LPP_ACCELEROMETER = 0x71, // 6 byte
    LPP_BAROMETER = 0x73, // 2 byte
    LPP_GYROMETER = 0x86, // 6 byte
    LPP_GPS_LOCATION = 0x88, // 9 byte    
};

/*!
 * Defines the application data transmission duty cycle. 5s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                            11000

/*!
 * Defines a random delay for application data transmission duty cycle. 1s,
 * value in [ms].
 */
#define APP_TX_DUTYCYCLE_RND                        1000

/*!
 * Default datarate
 */
#define LORAWAN_DEFAULT_DATARATE                    DR_0

/*!
 * LoRaWAN confirmed messages
 */
#define LORAWAN_CONFIRMED_MSG_ON                    false

/*!
 * LoRaWAN Adaptive Data Rate
 *
 * \remark Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_ON                              0

#if defined( REGION_EU868 )

#include "LoRaMacTest.h"

/*!
 * LoRaWAN ETSI duty cycle control enable/disable
 *
 * \remark Please note that ETSI mandates duty cycled transmissions. Use only for test purposes
 */
#define LORAWAN_DUTYCYCLE_ON                        true

#define USE_SEMTECH_DEFAULT_CHANNEL_LINEUP          1

#if( USE_SEMTECH_DEFAULT_CHANNEL_LINEUP == 1 )

#define LC4                { 867100000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC5                { 867300000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC6                { 867500000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC7                { 867700000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC8                { 867900000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC9                { 868800000, 0, { ( ( DR_7 << 4 ) | DR_7 ) }, 2 }
#define LC10               { 868300000, 0, { ( ( DR_6 << 4 ) | DR_6 ) }, 1 }

#endif

#endif

/*!
 * LoRaWAN application port
 */
#define LORAWAN_APP_PORT                            2

/*!
 * User application data buffer size
 */
#if defined( REGION_CN470 ) || defined( REGION_CN779 ) || defined( REGION_EU433 ) || defined( REGION_EU868 ) || defined( REGION_IN865 ) || defined( REGION_KR920 )
#define LORAWAN_APP_DATA_SIZE                       16
#elif defined( REGION_AS923 ) || defined( REGION_AU915 ) || defined( REGION_US915 ) || defined( REGION_US915_HYBRID )
#define LORAWAN_APP_DATA_SIZE                       11
#else

#error "Please define a region in the compiler options."

#endif

static uint8_t DevEui[] = LORAWAN_DEVICE_EUI;
static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;

#if( OVER_THE_AIR_ACTIVATION == 0 )

static uint8_t NwkSKey[] = LORAWAN_NWKSKEY;
static uint8_t AppSKey[] = LORAWAN_APPSKEY;

/*!
 * Device address
 */
static uint32_t DevAddr = LORAWAN_DEVICE_ADDRESS;

#endif

/*!
 * Application port
 */
static uint8_t AppPort = LORAWAN_APP_PORT;

/*!
 * User application data size
 */
static uint8_t AppDataSize = LORAWAN_APP_DATA_SIZE;

/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_MAX_SIZE                           242

/*!
 * User application data
 */
static uint8_t AppData[LORAWAN_APP_DATA_MAX_SIZE];

/*!
 * Indicates if the node is sending confirmed or unconfirmed messages
 */
static uint8_t IsTxConfirmed = LORAWAN_CONFIRMED_MSG_ON;

/*!
 * Defines the application data transmission duty cycle
 */
static uint32_t TxDutyCycleTime;

/*!
 * Timer to handle the application data transmission duty cycle
 */
//static TimerEvent_t TxNextPacketTimer;
static TimerHandle_t TxNextPacketTimer;

/*!
 * Specifies the state of the application LED
 */
static bool AppLedStateOn = false;

/*!
 * Timer to handle the state of LED1
 */
//static TimerEvent_t Led1Timer;
static TimerHandle_t Led1Timer;

/*!
 * Timer to handle the state of LED2
 */
//static TimerEvent_t Led2Timer;
static TimerHandle_t Led2Timer;
#if 0
/*!
 * Timer to handle the state of LED4
 */
//static TimerEvent_t Led4Timer;
static TimerHandle_t Led4Timer;
#endif
/*!
 * Indicates if a new packet can be sent
 */
static bool NextTx = true;

/*!
 * Device states
 */
static enum eDeviceState
{
    DEVICE_STATE_INIT,
    DEVICE_STATE_JOIN,
    DEVICE_STATE_SEND,
    DEVICE_STATE_CYCLE,
    DEVICE_STATE_SLEEP
}DeviceState;

/*!
 * LoRaWAN compliance tests support data
 */
struct ComplianceTest_s
{
    bool Running;
    uint8_t State;
    bool IsTxConfirmed;
    uint8_t AppPort;
    uint8_t AppDataSize;
    uint8_t *AppDataBuffer;
    uint16_t DownLinkCounter;
    bool LinkCheck;
    uint8_t DemodMargin;
    uint8_t NbGateways;
}ComplianceTest;

/*!
 * Reads the raw temperature
 * \retval temperature New raw temperature reading in 2's complement format
 */
int8_t RadioGetRawTemp(void)
{
    //uint64_t be = system_get_rtc_time();
    ESP_LOGW(TAG_NODE, "Enter %s", __func__);
    int8_t temp = 0;
    uint8_t preOpMode;

    // Save current Operation Mode
    preOpMode = SX1276Read(REG_OPMODE);

    // Pass through LoRa sleep only necessary if reading temperature while in Lora Mode
    if ((preOpMode & RFLR_OPMODE_LONGRANGEMODE_ON) == RFLR_OPMODE_LONGRANGEMODE_ON)
    {
        SX1276Write(REG_OPMODE, RFLR_OPMODE_SLEEP);
    }

    // Put device in FSK Sleep Mode
    SX1276Write(REG_OPMODE, RF_OPMODE_SLEEP);
    // Put device in FSK RxSynth
    SX1276Write(REG_OPMODE, RF_OPMODE_SYNTHESIZER_RX);
    // Enable Temperature reading
    uint8_t reg = SX1276Read(REG_IMAGECAL);
    reg = (reg & RF_IMAGECAL_TEMPMONITOR_MASK) | RF_IMAGECAL_TEMPMONITOR_ON;
    SX1276Write(REG_IMAGECAL, reg);

    // Wait 150us
    uint64_t st = system_get_rtc_time();
    while( ( system_get_rtc_time() - st) < 150 );

    // Disable Temperature reading
    reg = SX1276Read(REG_IMAGECAL);
    reg = (reg & RF_IMAGECAL_TEMPMONITOR_MASK) | RF_IMAGECAL_TEMPMONITOR_OFF;
    SX1276Write(REG_IMAGECAL, reg);

    // Put device in FSK Sleep Mode
    SX1276Write(REG_OPMODE, RF_OPMODE_SLEEP);

    // Read temperature
    reg = SX1276Read(REG_TEMP);
    if ((reg & 0x80) == 0x80)
    {
        temp = 255 - reg;
    }
    else
    {
        temp = reg;
        temp *= -1;
    }

    // We were in LoRa Mode prior to the temperature reading
    if ((preOpMode & RFLR_OPMODE_LONGRANGEMODE_ON) == RFLR_OPMODE_LONGRANGEMODE_ON)
    {
        SX1276Write(REG_OPMODE, RFLR_OPMODE_SLEEP);
    }

    // Reload previous Mode
    SX1276Write(REG_OPMODE, preOpMode);
    //uint64_t en = system_get_rtc_time() - be;
    //ESP_LOGI(TAG_NODE, "Leave %s, elapsed time:%llu(us)", __func__, en);
    return temp;
}

int8_t RadioGetTemp(int8_t compensationFactor)
{
    return RadioGetRawTemp() + compensationFactor;
}

/*!
 * \brief   Prepares the payload of the frame
 */
static void PrepareTxFrame( uint8_t port )
{
    ESP_LOGW(TAG_NODE, "Enter %s, port=%d", __func__, port);
    switch( port )
    {
    case 2:
        {
#if defined( REGION_CN470 ) || defined( REGION_CN779 ) || defined( REGION_EU433 ) || defined( REGION_EU868 ) || defined( REGION_IN865 ) || defined( REGION_KR920 )
#if 0
            uint16_t pressure = 10095;
            int16_t temperature = 208;// 0xD0
            int16_t altitudeGps = 100; // in m
            uint8_t Humidity = 41;
            uint16_t Illuminance = 209;

            AppData[0] = 0; // Data Channel
            AppData[1] = LPP_TEMPERATURE_SENSOR;
            //temperature *= 10;
            AppData[2] = ( temperature >> 8 ) & 0xFF;
            AppData[3] = temperature & 0xFF;

            AppData[4] = 1; // Data Channel
            AppData[5] = LPP_HUMIDITY_SENSOR;
            AppData[6] = Humidity;
            
            AppData[7] = 2; // Data Channel
            AppData[8] = LPP_BAROMETER;
            AppData[9] = ( pressure >> 8 ) & 0xFF;
            AppData[10] = pressure & 0xFF;

            AppData[11] = 3; // Data Channel
            AppData[12] = LPP_ILLUMINANCE_SENSOR;
            AppData[13] = ( Illuminance >> 8 ) & 0xFF;
            AppData[14] = Illuminance & 0xFF;
#elif 1
            uint16_t pressure = 0;
            int16_t altitudeBar = 0;
            int16_t temperature = 0;
            int32_t latitude, longitude = 0;
            int16_t altitudeGps = 0xFFFF;
            uint8_t batteryLevel = 0;

            pressure = 1000;//( uint16_t )( MPL3115ReadPressure( ) / 10 );             // in hPa / 10
            temperature = 19;//( int16_t )( MPL3115ReadTemperature( ) * 100 );       // in �C * 100
            altitudeBar = 96;//( int16_t )( MPL3115ReadAltitude( ) * 10 );           // in m * 10
            batteryLevel = BoardGetBatteryLevel( );                             // 1 (very low) to 254 (fully charged)
            //GpsGetLatestGpsPositionBinary( &latitude, &longitude );
            latitude = 39.979097;
            longitude = 116.45015;
            altitudeGps = 100;//GpsGetLatestGpsAltitude( );                           // in m

            AppData[0] = AppLedStateOn;
            AppData[1] = ( pressure >> 8 ) & 0xFF;
            AppData[2] = pressure & 0xFF;
            AppData[3] = ( temperature >> 8 ) & 0xFF;
            AppData[4] = temperature & 0xFF;
            AppData[5] = ( altitudeBar >> 8 ) & 0xFF;
            AppData[6] = altitudeBar & 0xFF;
            AppData[7] = batteryLevel;
            #if 0
            AppData[8] = ( latitude >> 16 ) & 0xFF;
            AppData[9] = ( latitude >> 8 ) & 0xFF;
            AppData[10] = latitude & 0xFF;
            AppData[11] = ( longitude >> 16 ) & 0xFF;
            AppData[12] = ( longitude >> 8 ) & 0xFF;
            AppData[13] = longitude & 0xFF;
            AppData[14] = ( altitudeGps >> 8 ) & 0xFF;
            AppData[15] = altitudeGps & 0xFF;
            #endif
            #if 1
            uint64_t val = system_get_rtc_time();
            AppData[8] = val >> 56;
            AppData[9] = val >> 48;
            AppData[10] = val >> 40;
            AppData[11] = val >> 32;
            AppData[12] = val >> 24;
            AppData[13] = val >> 16;
            AppData[14] = val >> 8;
            AppData[15] = val;
            #endif
#endif
#elif defined( REGION_AS923 ) || defined( REGION_AU915 ) || defined( REGION_US915 ) || defined( REGION_US915_HYBRID )
            int16_t temperature = 0;
            int32_t latitude, longitude = 0;
            uint16_t altitudeGps = 0xFFFF;
            uint8_t batteryLevel = 0;

            temperature = ( int16_t )( MPL3115ReadTemperature( ) * 100 );       // in �C * 100

            batteryLevel = BoardGetBatteryLevel( );                             // 1 (very low) to 254 (fully charged)
            GpsGetLatestGpsPositionBinary( &latitude, &longitude );
            altitudeGps = GpsGetLatestGpsAltitude( );                           // in m

            AppData[0] = AppLedStateOn;
            AppData[1] = temperature;                                           // Signed degrees celsius in half degree units. So,  +/-63 C
            AppData[2] = batteryLevel;                                          // Per LoRaWAN spec; 0=Charging; 1...254 = level, 255 = N/A
            AppData[3] = ( latitude >> 16 ) & 0xFF;
            AppData[4] = ( latitude >> 8 ) & 0xFF;
            AppData[5] = latitude & 0xFF;
            AppData[6] = ( longitude >> 16 ) & 0xFF;
            AppData[7] = ( longitude >> 8 ) & 0xFF;
            AppData[8] = longitude & 0xFF;
            AppData[9] = ( altitudeGps >> 8 ) & 0xFF;
            AppData[10] = altitudeGps & 0xFF;
#endif
        }
        break;
    case 224:
        if( ComplianceTest.LinkCheck == true )
        {
            ComplianceTest.LinkCheck = false;
            AppDataSize = 3;
            AppData[0] = 5;
            AppData[1] = ComplianceTest.DemodMargin;
            AppData[2] = ComplianceTest.NbGateways;
            ComplianceTest.State = 1;
        }
        else
        {
            switch( ComplianceTest.State )
            {
            case 4:
                ComplianceTest.State = 1;
                break;
            case 1:
                AppDataSize = 2;
                AppData[0] = ComplianceTest.DownLinkCounter >> 8;
                AppData[1] = ComplianceTest.DownLinkCounter;
                break;
            }
        }
        break;
    default:
        break;
    }
}

/*!
 * \brief   Prepares the payload of the frame
 *
 * \retval  [0: frame could be send, 1: error]
 */
static bool SendFrame( void )
{
    ESP_LOGW(TAG_NODE, "Enter %s", __func__);
    McpsReq_t mcpsReq;
    LoRaMacTxInfo_t txInfo;

    if( LoRaMacQueryTxPossible( AppDataSize, &txInfo ) != LORAMAC_STATUS_OK )
    {
        // Send empty frame in order to flush MAC commands
        mcpsReq.Type = MCPS_UNCONFIRMED;
        mcpsReq.Req.Unconfirmed.fBuffer = NULL;
        mcpsReq.Req.Unconfirmed.fBufferSize = 0;
        mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
    }
    else
    {
        if( IsTxConfirmed == false )
        {
            mcpsReq.Type = MCPS_UNCONFIRMED;
            mcpsReq.Req.Unconfirmed.fPort = AppPort;
            mcpsReq.Req.Unconfirmed.fBuffer = AppData;
            mcpsReq.Req.Unconfirmed.fBufferSize = AppDataSize;
            mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
        }
        else
        {
            mcpsReq.Type = MCPS_CONFIRMED;
            mcpsReq.Req.Confirmed.fPort = AppPort;
            mcpsReq.Req.Confirmed.fBuffer = AppData;
            mcpsReq.Req.Confirmed.fBufferSize = AppDataSize;
            mcpsReq.Req.Confirmed.NbTrials = 8;
            mcpsReq.Req.Confirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
        }
    }

    if( LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK )
    {
        return false;
    }
    return true;
}

/*!
 * \brief Function executed on TxNextPacket Timeout event
 */
static void OnTxNextPacketTimerEvent( TimerHandle_t xTimer )
{
    ESP_LOGI(TAG_NODE, "Enter %s", __func__);
    MibRequestConfirm_t mibReq;
    LoRaMacStatus_t status;

    //TimerStop( &TxNextPacketTimer );
    xTimerStop( TxNextPacketTimer, portMAX_DELAY );

    mibReq.Type = MIB_NETWORK_JOINED;
    status = LoRaMacMibGetRequestConfirm( &mibReq );

    if( status == LORAMAC_STATUS_OK )
    {
        enum eDeviceState state;
        if( mibReq.Param.IsNetworkJoined == true )
        {
            state = DEVICE_STATE_SEND;
            NextTx = true;
        }
        else
        {
            state = DEVICE_STATE_JOIN;
        }
        xQueueSend(g_lora_mac_queue, &state, 0);
    }
}

/*!
 * \brief Function executed on Led 1 Timeout event
 */
static void OnLed1TimerEvent( TimerHandle_t xTimer)
{
    #if 0
    TimerStop( &Led1Timer );
    // Switch LED 1 OFF
    GpioWrite( &Led1, 1 );
    #endif
    gpio_set_level(LED_BLE, LED_OFF);
    vTaskDelay(50 / portTICK_PERIOD_MS);
    gpio_set_level(LED_BLE, LED_ON);
    vTaskDelay(50 / portTICK_PERIOD_MS);
    gpio_set_level(LED_BLE, LED_OFF);
}

/*!
 * \brief Function executed on Led 2 Timeout event
 */
static void OnLed2TimerEvent( TimerHandle_t xTimer )
{
    #if 0
    TimerStop( &Led2Timer );
    // Switch LED 2 OFF
    GpioWrite( &Led2, 1 );
    #endif
    vTaskDelay(550 / portTICK_PERIOD_MS);
    gpio_set_level(LED_BLE, LED_OFF);
}

#if 0
/*!
 * \brief Function executed on Led 4 Timeout event
 */
static void OnLed4TimerEvent( TimerHandle_t xTimer )
{
    TimerStop( &Led4Timer );
    // Switch LED 4 OFF
    GpioWrite( &Led4, 1 );
}
#endif

/*!
 * \brief   MCPS-Confirm event function
 *
 * \param   [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void McpsConfirm_CB( McpsConfirm_t *mcpsConfirm )
{
    ESP_LOGW(TAG_NODE, "Enter %s, DeviceState:%d, Status : %d, McpsRequest=%d", __func__, DeviceState, mcpsConfirm->Status, mcpsConfirm->McpsRequest);
    if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
    {
        switch( mcpsConfirm->McpsRequest )
        {
            case MCPS_UNCONFIRMED:
            {
                // Check Datarate
                // Check TxPower
                break;
            }
            case MCPS_CONFIRMED:
            {
                // Check Datarate
                // Check TxPower
                // Check AckReceived
                // Check NbTrials
                break;
            }
            case MCPS_PROPRIETARY:
            {
                break;
            }
            default:
                break;
        }
#if 0
        // Switch LED 1 ON
        GpioWrite( &Led1, 0 );
        TimerStart( &Led1Timer );
#endif
        gpio_set_level(LED_BLE, LED_ON);
        xTimerStart(Led1Timer, 100 / portTICK_PERIOD_MS);
    }
    NextTx = true;
}

/*!
 * \brief   MCPS-Indication event function
 *
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
static void McpsIndication_CB( McpsIndication_t *mcpsIndication )
{
    ESP_LOGW(TAG_NODE, "Enter %s, DeviceState:%d, Status : %d, McpsIndication=%d", __func__, DeviceState, mcpsIndication->Status, mcpsIndication->McpsIndication);
    if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK )
    {
        return;
    }
    enum eDeviceState state = DeviceState;
    switch( mcpsIndication->McpsIndication )
    {
        case MCPS_UNCONFIRMED:
        {
            break;
        }
        case MCPS_CONFIRMED:
        {
            break;
        }
        case MCPS_PROPRIETARY:
        {
            break;
        }
        case MCPS_MULTICAST:
        {
            break;
        }
        default:
            break;
    }

    // Check Multicast
    // Check Port
    // Check Datarate
    // Check FramePending
    // Check Buffer
    // Check BufferSize
    // Check Rssi
    // Check Snr
    // Check RxSlot

    if( ComplianceTest.Running == true )
    {
        ComplianceTest.DownLinkCounter++;
    }

    dumpBytes(mcpsIndication->Buffer, mcpsIndication->BufferSize);
    ESP_LOGE(TAG_NODE, "Port=%d, RxDatarate=%d, FramePending=%d, RxData=%d, Rssi=%d, Snr=%d, RxSlot=%d, AckReceived=%d, DownLinkCounter=%d",
                mcpsIndication->Port, mcpsIndication->RxDatarate, mcpsIndication->FramePending,
                mcpsIndication->RxData, mcpsIndication->Rssi, mcpsIndication->Snr, mcpsIndication->RxSlot,
                mcpsIndication->AckReceived, mcpsIndication->DownLinkCounter);
    if( mcpsIndication->RxData == true )
    {
        switch( mcpsIndication->Port )
        {
        case 1: // The application LED can be controlled on port 1 or 2
        case 2:
            if( mcpsIndication->BufferSize == 1 )
            {
                AppLedStateOn = mcpsIndication->Buffer[0] & 0x01;
                //GpioWrite( &Led3, ( ( AppLedStateOn & 0x01 ) != 0 ) ? 0 : 1 );
            }
            break;
        case 224:
            if( ComplianceTest.Running == false )
            {
                // Check compliance test enable command (i)
                if( ( mcpsIndication->BufferSize == 4 ) &&
                    ( mcpsIndication->Buffer[0] == 0x01 ) &&
                    ( mcpsIndication->Buffer[1] == 0x01 ) &&
                    ( mcpsIndication->Buffer[2] == 0x01 ) &&
                    ( mcpsIndication->Buffer[3] == 0x01 ) )
                {
                    IsTxConfirmed = false;
                    AppPort = 224;
                    AppDataSize = 2;
                    ComplianceTest.DownLinkCounter = 0;
                    ComplianceTest.LinkCheck = false;
                    ComplianceTest.DemodMargin = 0;
                    ComplianceTest.NbGateways = 0;
                    ComplianceTest.Running = true;
                    ComplianceTest.State = 1;

                    MibRequestConfirm_t mibReq;
                    mibReq.Type = MIB_ADR;
                    mibReq.Param.AdrEnable = true;
                    LoRaMacMibSetRequestConfirm( &mibReq );

#if defined( REGION_EU868 )
                    LoRaMacTestSetDutyCycleOn( false );
#endif
                    //GpsStop( );
                }
            }
            else
            {
                ComplianceTest.State = mcpsIndication->Buffer[0];
                switch( ComplianceTest.State )
                {
                case 0: // Check compliance test disable command (ii)
                    IsTxConfirmed = LORAWAN_CONFIRMED_MSG_ON;
                    AppPort = LORAWAN_APP_PORT;
                    AppDataSize = LORAWAN_APP_DATA_SIZE;
                    ComplianceTest.DownLinkCounter = 0;
                    ComplianceTest.Running = false;

                    MibRequestConfirm_t mibReq;
                    mibReq.Type = MIB_ADR;
                    mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
                    LoRaMacMibSetRequestConfirm( &mibReq );
#if defined( REGION_EU868 )
                    LoRaMacTestSetDutyCycleOn( LORAWAN_DUTYCYCLE_ON );
#endif
                    //GpsStart( );
                    break;
                case 1: // (iii, iv)
                    AppDataSize = 2;
                    break;
                case 2: // Enable confirmed messages (v)
                    IsTxConfirmed = true;
                    ComplianceTest.State = 1;
                    break;
                case 3:  // Disable confirmed messages (vi)
                    IsTxConfirmed = false;
                    ComplianceTest.State = 1;
                    break;
                case 4: // (vii)
                    AppDataSize = mcpsIndication->BufferSize;

                    AppData[0] = 4;
                    for( uint8_t i = 1; i < MIN( AppDataSize, LORAWAN_APP_DATA_MAX_SIZE ); i++ )
                    {
                        AppData[i] = mcpsIndication->Buffer[i] + 1;
                    }
                    break;
                case 5: // (viii)
                    {
                        MlmeReq_t mlmeReq;
                        mlmeReq.Type = MLME_LINK_CHECK;
                        LoRaMacMlmeRequest( &mlmeReq );
                    }
                    break;
                case 6: // (ix)
                    {
                        MlmeReq_t mlmeReq;

                        // Disable TestMode and revert back to normal operation
                        IsTxConfirmed = LORAWAN_CONFIRMED_MSG_ON;
                        AppPort = LORAWAN_APP_PORT;
                        AppDataSize = LORAWAN_APP_DATA_SIZE;
                        ComplianceTest.DownLinkCounter = 0;
                        ComplianceTest.Running = false;

                        MibRequestConfirm_t mibReq;
                        mibReq.Type = MIB_ADR;
                        mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
                        LoRaMacMibSetRequestConfirm( &mibReq );
#if defined( REGION_EU868 )
                        LoRaMacTestSetDutyCycleOn( LORAWAN_DUTYCYCLE_ON );
#endif
                        //GpsStart( );

                        mlmeReq.Type = MLME_JOIN;

                        mlmeReq.Req.Join.DevEui = DevEui;
                        mlmeReq.Req.Join.AppEui = AppEui;
                        mlmeReq.Req.Join.AppKey = AppKey;
                        mlmeReq.Req.Join.NbTrials = 3;

                        LoRaMacMlmeRequest( &mlmeReq );
                        state = DEVICE_STATE_SLEEP;
                    }
                    break;
                case 7: // (x)
                    {
                        if( mcpsIndication->BufferSize == 3 )
                        {
                            MlmeReq_t mlmeReq;
                            mlmeReq.Type = MLME_TXCW;
                            mlmeReq.Req.TxCw.Timeout = ( uint16_t )( ( mcpsIndication->Buffer[1] << 8 ) | mcpsIndication->Buffer[2] );
                            LoRaMacMlmeRequest( &mlmeReq );
                        }
                        else if( mcpsIndication->BufferSize == 7 )
                        {
                            MlmeReq_t mlmeReq;
                            mlmeReq.Type = MLME_TXCW_1;
                            mlmeReq.Req.TxCw.Timeout = ( uint16_t )( ( mcpsIndication->Buffer[1] << 8 ) | mcpsIndication->Buffer[2] );
                            mlmeReq.Req.TxCw.Frequency = ( uint32_t )( ( mcpsIndication->Buffer[3] << 16 ) | ( mcpsIndication->Buffer[4] << 8 ) | mcpsIndication->Buffer[5] ) * 100;
                            mlmeReq.Req.TxCw.Power = mcpsIndication->Buffer[6];
                            LoRaMacMlmeRequest( &mlmeReq );
                        }
                        ComplianceTest.State = 1;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        default:
            break;
        }
        ESP_LOGW(TAG_NODE, "Leave %s, state:%d", __func__, state);
        if (DeviceState != state)
        {
            xQueueSend(g_lora_mac_queue, &state, 0);
        }
    }

    // Switch LED 2 ON for each received downlink
#if 0
    GpioWrite( &Led2, 0 );
    TimerStart( &Led2Timer );
#endif
    gpio_set_level(LED_BLE, LED_ON);
    xTimerStart(Led2Timer, 100 / portTICK_PERIOD_MS);
}

/*!
 * \brief   MLME-Confirm event function
 *
 * \param   [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void MlmeConfirm_CB( MlmeConfirm_t *mlmeConfirm )
{
    ESP_LOGW(TAG_NODE, "Enter %s, MlmeRequest:%d", __func__, mlmeConfirm->MlmeRequest);
    enum eDeviceState state = DeviceState;
    switch( mlmeConfirm->MlmeRequest )
    {
        case MLME_JOIN:
        {
            if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
            {
                // Status is OK, node has joined the network
                state = DEVICE_STATE_SEND;
            }
            else
            {
                // Join was not successful. Try to join again
                state = DEVICE_STATE_JOIN;
            }
            break;
        }
        case MLME_LINK_CHECK:
        {
            if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
            {
                // Check DemodMargin
                // Check NbGateways
                if( ComplianceTest.Running == true )
                {
                    ComplianceTest.LinkCheck = true;
                    ComplianceTest.DemodMargin = mlmeConfirm->DemodMargin;
                    ComplianceTest.NbGateways = mlmeConfirm->NbGateways;
                }
            }
            break;
        }
        default:
            break;
    }
    NextTx = true;
    ESP_LOGW(TAG_NODE, "Leave %s, state:%d", __func__, state);
    if (DeviceState != state)
    {
        xQueueSend(g_lora_mac_queue, &state, 0);
    }
}

#if 0
static void MlmeIndication( MlmeIndication_t *MlmeIndication )
{
    switch( MlmeIndication->MlmeIndication )
    {
        default:
            break;
    }
}
#endif

static void rhea_mac_monitor_task(void *pvParameters)
{
    uint8_t reg = 0x78;
    uint8_t reg1= 0x79;
    ESP_LOGW(TAG_NODE, "Enter %s", __func__);
    vTaskDelay(9004 / portTICK_PERIOD_MS);
    while (true)
    {
        
        //reg = SX1276Read(REG_LR_VERSION);
        reg = SX1276Read(REG_LR_MODEMCONFIG1);
        reg1 = SX1276Read(REG_LR_MODEMCONFIG2); 
        //ESP_LOGD(TAG_NODE, "REG_LR_VERSION=0x%02X, freememory=%d", reg, esp_get_free_heap_size());
        ESP_LOGE(TAG_NODE, "MODEMCONFIG1=0x%02X, MODEMCONFIG2=0x%02X, freememory=%d", reg, reg1, esp_get_free_heap_size());
        vTaskDelay(1004 / portTICK_PERIOD_MS);
        if (reg == 0x00) // Something was wrong!!!
        {
            esp_restart();
        }
        vTaskDelay(10004 / portTICK_PERIOD_MS);
    }
}

/**
 * Main application entry point.
 */
static void lora_mac_task( void *pvParameter )
{
    ESP_LOGI(TAG_NODE, "Enter %s", __func__);
    LoRaMacPrimitives_t LoRaMacPrimitives;
    LoRaMacCallback_t LoRaMacCallbacks;
    MibRequestConfirm_t mibReq;

    vTaskDelay(5004 / portTICK_PERIOD_MS);
    BoardInitMcu( );
    BoardInitPeriph( );

    DeviceState = DEVICE_STATE_INIT;

    while( 1 )
    {
        ESP_LOGW("Mac_task", "DeviceState : %d", DeviceState);
        //vTaskDelay(100 / portTICK_PERIOD_MS);
        switch( DeviceState )
        {
            case DEVICE_STATE_INIT:
            {
                LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm_CB;
                LoRaMacPrimitives.MacMcpsIndication = McpsIndication_CB;
                LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm_CB;
                //LoRaMacPrimitives.MacMlmeIndication = MlmeIndication;
                LoRaMacCallbacks.GetBatteryLevel = BoardGetBatteryLevel;
                //LoRaMacCallbacks.GetTemperatureLevel = MPL3115ReadTemperature;
#if defined( REGION_AS923 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_AS923 );
#elif defined( REGION_AU915 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_AU915 );
#elif defined( REGION_CN470 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_CN470 );
#elif defined( REGION_CN779 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_CN779 );
#elif defined( REGION_EU433 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU433 );
#elif defined( REGION_EU868 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU868 );
#elif defined( REGION_IN865 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_IN865 );
#elif defined( REGION_KR920 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_KR920 );
#elif defined( REGION_US915 )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_US915 );
#elif defined( REGION_US915_HYBRID )
                LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_US915_HYBRID );
#else
    #error "Please define a region in the compiler options."
#endif
#if 0
                TimerInit( &TxNextPacketTimer, OnTxNextPacketTimerEvent );

                TimerInit( &Led1Timer, OnLed1TimerEvent );
                TimerSetValue( &Led1Timer, 25 );

                TimerInit( &Led2Timer, OnLed2TimerEvent );
                TimerSetValue( &Led2Timer, 25 );

                TimerInit( &Led4Timer, OnLed4TimerEvent );
                TimerSetValue( &Led4Timer, 25 );
#endif
                Led1Timer = xTimerCreate("led1", (50 / portTICK_PERIOD_MS),
                              pdFALSE, (void*)NULL, OnLed1TimerEvent);
                Led2Timer = xTimerCreate("led2", (50 / portTICK_PERIOD_MS),
                              pdFALSE, (void*)NULL, OnLed2TimerEvent);
                TxNextPacketTimer = xTimerCreate("txNext", (5000 / portTICK_PERIOD_MS),
                              pdFALSE, (void*)NULL, OnTxNextPacketTimerEvent);
                mibReq.Type = MIB_ADR;
                mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_PUBLIC_NETWORK;
                mibReq.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
                LoRaMacMibSetRequestConfirm( &mibReq );

#if defined( REGION_EU868 )
                LoRaMacTestSetDutyCycleOn( LORAWAN_DUTYCYCLE_ON );

#if( USE_SEMTECH_DEFAULT_CHANNEL_LINEUP == 1 )
                LoRaMacChannelAdd( 3, ( ChannelParams_t )LC4 );
                LoRaMacChannelAdd( 4, ( ChannelParams_t )LC5 );
                LoRaMacChannelAdd( 5, ( ChannelParams_t )LC6 );
                LoRaMacChannelAdd( 6, ( ChannelParams_t )LC7 );
                LoRaMacChannelAdd( 7, ( ChannelParams_t )LC8 );
                LoRaMacChannelAdd( 8, ( ChannelParams_t )LC9 );
                LoRaMacChannelAdd( 9, ( ChannelParams_t )LC10 );

                mibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
                mibReq.Param.Rx2DefaultChannel = ( Rx2ChannelParams_t ){ 869525000, DR_3 };
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_RX2_CHANNEL;
                mibReq.Param.Rx2Channel = ( Rx2ChannelParams_t ){ 869525000, DR_3 };
                LoRaMacMibSetRequestConfirm( &mibReq );
#endif

#endif
                DeviceState = DEVICE_STATE_JOIN;
                break;
            }
            case DEVICE_STATE_JOIN:
            {
#if( OVER_THE_AIR_ACTIVATION != 0 )
                MlmeReq_t mlmeReq;

                // Initialize LoRaMac device unique ID
                //BoardGetUniqueId( DevEui );

                mlmeReq.Type = MLME_JOIN;

                mlmeReq.Req.Join.DevEui = DevEui;
                mlmeReq.Req.Join.AppEui = AppEui;
                mlmeReq.Req.Join.AppKey = AppKey;
                mlmeReq.Req.Join.NbTrials = 3;

                if( NextTx == true )
                {
                    LoRaMacMlmeRequest( &mlmeReq );
                }
                DeviceState = DEVICE_STATE_SLEEP;
#else
                // Choose a random device address if not already defined in Commissioning.h
                if( DevAddr == 0 )
                {
                    #if 0
                    // Random seed initialization
                    srand1( BoardGetRandomSeed( ) );
                    // Choose a random device address
                    DevAddr = randr( 0, 0x01FFFFFF );
                    #endif
                    #if 0
                    //DevAddr = esp_random() % 0x01FFFFFF;
                    DevAddr = randr(0, 0x01FFFFFF);
                    #else
                    
                    #endif
                    
                }
                ESP_LOGI(TAG_NODE, "DevAddr = %u(0x%x)", DevAddr, DevAddr);

                mibReq.Type = MIB_NET_ID;
                mibReq.Param.NetID = LORAWAN_NETWORK_ID;
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_DEV_ADDR;
                mibReq.Param.DevAddr = DevAddr;
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_NWK_SKEY;
                mibReq.Param.NwkSKey = NwkSKey;
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_APP_SKEY;
                mibReq.Param.AppSKey = AppSKey;
                LoRaMacMibSetRequestConfirm( &mibReq );

                mibReq.Type = MIB_NETWORK_JOINED;
                mibReq.Param.IsNetworkJoined = true;
                LoRaMacMibSetRequestConfirm( &mibReq );

                DeviceState = DEVICE_STATE_SEND;
#endif
                break;
            }
            case DEVICE_STATE_SEND:
            {
                if( NextTx == true )
                {
                    PrepareTxFrame( AppPort );

                    NextTx = SendFrame( );
                }
                if( ComplianceTest.Running == true )
                {
                    // Schedule next packet transmission
                    TxDutyCycleTime = 5000; // 5000 ms
                }
                else
                {
                    // Schedule next packet transmission
                    TxDutyCycleTime = APP_TX_DUTYCYCLE + randr( -APP_TX_DUTYCYCLE_RND, APP_TX_DUTYCYCLE_RND );
                    //TxDutyCycleTime = APP_TX_DUTYCYCLE + esp_random(); ??????????????
                }
                ESP_LOGE(TAG_NODE, "TxDutyCycleTime = %u", TxDutyCycleTime);
                DeviceState = DEVICE_STATE_CYCLE;
                break;
            }
            case DEVICE_STATE_CYCLE:
            {
                DeviceState = DEVICE_STATE_SLEEP;

                // Schedule next packet transmission
                #if 0
                TimerSetValue( &TxNextPacketTimer, TxDutyCycleTime );
                TimerStart( &TxNextPacketTimer );
                #endif
                xTimerChangePeriod(TxNextPacketTimer, TxDutyCycleTime / portTICK_PERIOD_MS, portMAX_DELAY);
                xTimerStart( TxNextPacketTimer, 100 / portTICK_PERIOD_MS );
                break;
            }
            case DEVICE_STATE_SLEEP:
            {
                ESP_LOGE(TAG_NODE, "DEVICE_STATE_SLEEP");
                // Wake up through events
                #if 0
                TimerLowPowerHandler( );
                #endif
                xQueueReceive(g_lora_mac_queue, &DeviceState, portMAX_DELAY);
                //ESP_LOGE(TAG_NODE, "DeviceState = %d", DeviceState);
                break;
            }
            default:
            {
                DeviceState = DEVICE_STATE_INIT;
                break;
            }
        }
        #if 0
        if( GpsGetPpsDetectedState( ) == true )
        {
            // Switch LED 4 ON
            GpioWrite( &Led4, 0 );
            TimerStart( &Led4Timer );
        }
        #endif
    }
}

void lora_mac_main(void)
{
    ESP_LOGW(TAG_NODE, "Enter %s, sw version: %s, freememory:%d, idf:%s", __func__, DIONE_SW_VERSION, esp_get_free_heap_size(), esp_get_idf_version());

    g_lora_mac_queue = xQueueCreate(10, sizeof(uint8_t));
    xTaskCreate(lora_mac_task, "mac_lora", 4096, NULL, 22, NULL);
    //xTaskCreate(rhea_mac_monitor_task, "mac_monitor", 2048, NULL, 2, NULL);

    ESP_LOGW(TAG_NODE, "Leave %s", __func__);
}

