#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "Dione_config.h"
#include "radio.h"
#include "sx1276.h"
#include "sx1276-board.h"
#include "mqtt.h"

extern mqtt_client *g_mqtt_client;
extern uint64_t system_get_rtc_time(void);
extern char gCID[16];
static const char *TAG_RHEA = "Rhea_main";

//#define RF_FREQUENCY                                470500000 // Hz
//#define RF_FREQUENCY                                433000000 // Hz
//#define RF_FREQUENCY                                433050000 // No work
//#define RF_FREQUENCY                                433125000 // Work
//#define RF_FREQUENCY                                433250000 // No work
//#define RF_FREQUENCY                                432875000 // Work
//#define RF_FREQUENCY                                433000010 // Work
volatile uint32_t gFrequency = 470500000;
uint32_t gChannels[2048];
uint16_t gChannelIdx = 368;
volatile bool gIfChangeFreq = false;
volatile bool gLoraAutoTest = false;
uint16_t gRxDoneCounter = 0;
bool gChanggingFrequency = false;
#define RX_DONE_COUNTER_MAX 10

static RadioModems_t modem = MODEM_LORA;        //Radio modem to be used [0: FSK, 1: LoRa]
static int8_t power = 20;          //Sets the output power [dBm]
static uint32_t fdev = 0;          //Sets the frequency deviation (FSK only)
                     //          FSK : [Hz]
                     //          LoRa: 0
static uint32_t bandwidth = 0;    //Sets the bandwidth (LoRa only)
                     //          FSK : 0
                     //          LoRa: [0: 125 kHz, 1: 250 kHz,
                     //                 2: 500 kHz, 3: Reserved]
static uint32_t datarate = 12;     //Sets the Datarate
                     //          FSK : 600..300000 bits/s
                     //          LoRa: [6: 64, 7: 128, 8: 256, 9: 512,
                     //                10: 1024, 11: 2048, 12: 4096  chips]
static uint8_t coderate = 1;     //Sets the coding rate (LoRa only)
                    //           FSK : N/A ( set to 0 )
                    //           LoRa: [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]
static uint16_t preambleLen = 8;  //Sets the preamble length
                     //          FSK : Number of bytes
                     //          LoRa: Length in symbols (the hardware adds 4 more symbols)
static uint8_t fixLen = 0;       //Fixed length packets [0: variable, 1: fixed]
static uint8_t crcOn = 1;        //Enables disables the CRC [0: OFF, 1: ON]
static uint8_t freqHopOn = 0;    //Enables disables the intra-packet frequency hopping
                    //           FSK : N/A ( set to 0 )
                    //           LoRa: [0: OFF, 1: ON]
static uint8_t hopPeriod = 0;    //Number of symbols between each hop
                     //         FSK : N/A ( set to 0 )
                     //         LoRa: Number of symbols
static uint8_t iqInverted = 0;   //Inverts IQ signals (LoRa only)
                    //           FSK : N/A ( set to 0 )
                    //           LoRa: [0: not inverted, 1: inverted]
static uint32_t txTimeout = 8000;      //Transmission timeout [ms]
/* The following value SetRxConfig will only use */
static uint32_t bandwidthAfc = 0; //Sets the AFC Bandwidth (FSK only)
                      //         FSK : >= 2600 and <= 250000 Hz
                      //         LoRa: N/A ( set to 0 )
static uint16_t symbTimeout = 1023;  //Sets the RxSingle timeout value, max length is 1023 (10 bit)
                     //          FSK : timeout in number of bytes
                     //          LoRa: timeout in symbols
static uint8_t payloadLen = 0;    //Sets payload length when fixed length is used

static uint8_t rxContinuous = 1;  //Sets the reception in continuous mode
                     //           [false: single mode, true: continuous mode]
#define RX_TIMEOUT_VALUE                            2000 // ms
#define BUFFER_SIZE                                 64 // Define the payload size here

static const uint8_t PingMsg[] = "LPING";
static const uint8_t PongMsg[] = "LPONG";

#define PAY_LOAD_LENGTH 9
#define PING_PONG_LEN 5
static uint16_t BufferSize = PAY_LOAD_LENGTH;
static uint8_t Buffer[BUFFER_SIZE];

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

static int32_t RssiValue = 0;
static int32_t SnrValue = 0;
static int averageRssiValue = 0;
static int averageSnrValue = 0;

static uint16_t txTimeOutCount = 0;
static uint16_t rxTimeOutCount = 0;

/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

static xQueueHandle g_led_toggle_queue;
static xQueueHandle g_pingpang_queue;

esp_err_t rhea_nvs_init(void)
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err;
    err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READONLY, &my_handle);
    err = nvs_get_u32(my_handle, "rhea.freq", (uint32_t*)&gFrequency);
    err = nvs_get_u16(my_handle, "rhea.idx", &gChannelIdx);
    
    nvs_close(my_handle);
    return err;
}

esp_err_t rhea_nvs_save_frequency(void)
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u32(my_handle, "rhea.freq", gFrequency);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t rhea_nvs_save_idx(void)
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u16(my_handle, "rhea.idx", gChannelIdx);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static void dumpBytes(const uint8_t *data, size_t count)
{
    for (uint32_t i = 0; i < count; ++i)
    {
        if (i % 16 == 0)
        {
            printf("%08x    ", i);
        }
        printf("0x%02x, ", data[i]);
        if ((i + 1) % 16 == 0)
        {
            printf("\n");
        }
    }
    printf("\r\n");
}

uint32_t TimerGetCurrentTime(void)
{
    return system_get_time()/1000;
}

uint32_t TimerGetElapsedTime( uint32_t eventInTime )
{
    uint32_t elapsedTime = 0;

    // Needed at boot, cannot compute with 0 or elapsed time will be equal to current time
    if( eventInTime == 0 )
    {
        return 0;
    }

    elapsedTime = system_get_time() / 1000;

    if( elapsedTime < eventInTime )
    { // roll over of the counter
        return( elapsedTime + ( 0xFFFFFFFF - eventInTime ) );
    }
    else
    {
        return( elapsedTime - eventInTime );
    }
}

uint32_t TimerGetElapsedTime_us( uint32_t eventInTime )
{
    uint32_t elapsedTime = 0;

    // Needed at boot, cannot compute with 0 or elapsed time will be equal to current time
    if( eventInTime == 0 )
    {
        return 0;
    }

    elapsedTime = system_get_time();

    if( elapsedTime < eventInTime )
    { // roll over of the counter
        return( elapsedTime + ( 0xFFFFFFFF - eventInTime ) );
    }
    else
    {
        return( elapsedTime - eventInTime );
    }
}

void DelayMs( uint32_t ms )
{
    uint64_t startTick = system_get_rtc_time();
    uint64_t us = ms * 1000;
    while( ( system_get_rtc_time() - startTick ) < us );
}

// Delay 10 ~ 4095 ms
void DelayRandomMs(void)
{
    uint64_t startTick = system_get_rtc_time();
    uint16_t randomMS = esp_random();
    randomMS = randomMS & 0x0FFF;
    if (randomMS > 0 && randomMS < 10)
    {
        randomMS = 10;
    }
    uint64_t us = randomMS * 1000;
    //vTaskDelay(randomMS / portTICK_PERIOD_MS);
    while( ( system_get_rtc_time() - startTick ) < us );
}

static void rhea_monitor_task(void *pvParameters)
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    vTaskDelay(10004 / portTICK_PERIOD_MS);
    while (true)
    {
        uint8_t reg = 0x78;
        reg = SX1276Read(REG_LR_VERSION);
        ESP_LOGD(TAG_RHEA, "REG_LR_VERSION=0x%02X, freememory=%d, gFrequency = %u", reg, esp_get_free_heap_size(), gFrequency);
        ESP_LOGD(TAG_RHEA, "Timeout rx:%d, tx:%d", rxTimeOutCount, txTimeOutCount);
        vTaskDelay(1004 / portTICK_PERIOD_MS);
        if (reg == 0x00) // Something was wrong!!!
        {
            esp_restart();
        }
        vTaskDelay(5004 / portTICK_PERIOD_MS);
    }
}

static void led_Toggle_task(void *pvParameters)
{
    //uint8_t led;
    uint8_t time;
    g_led_toggle_queue = xQueueCreate(10, sizeof(uint8_t));
    while (1)
    {
        if (xQueueReceive(g_led_toggle_queue, &time, portMAX_DELAY))
        {
            //ESP_LOGI(TAG_RHEA, "led_Toggle_task:%d", time);
            vTaskDelay(101 / portTICK_PERIOD_MS);
            gpio_set_level(LED_BLE, LED_ON);
            if (time == 0)
            {
                vTaskDelay(30 / portTICK_PERIOD_MS);
            }
            else if (time == 1)
            {
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(70 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
            }
            else if (time == 2)
            {
                vTaskDelay(700 / portTICK_PERIOD_MS);
            }
            else if (time == 3)
            {
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(60 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(60 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
            }
            gpio_set_level(LED_BLE, LED_OFF);
        }        
    }
}

static void LedToggle(uint8_t led, uint8_t time)
{
    xQueueSend(g_led_toggle_queue, &time, 0);
}

static void LedOff(uint8_t led)
{
    gpio_set_level(led, LED_OFF);
}

// Tx timeout shouldn't happen.
// But it has been observed that when it happens it is a result of a corrupted SPI transfer
// it depends on the platform design.
//
// The workaround is to put the radio in a known state. Thus, we re-initialize it.
static void reinit_lora(void)
{
    Radio.SetChannel( gFrequency );
    Radio.SetTxConfig( modem, power, fdev, bandwidth,
                                   datarate, coderate,
                                   preambleLen, fixLen,
                                   crcOn, freqHopOn, hopPeriod, iqInverted, txTimeout);

    Radio.SetRxConfig( modem, bandwidth, datarate,
                                   coderate, bandwidthAfc, preambleLen,
                                   symbTimeout, fixLen, payloadLen,
                                   crcOn, freqHopOn, hopPeriod, iqInverted, rxContinuous);
}

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
static void OnTxDone( void )
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    Radio.Sleep( );
    States_t state = TX;
    xQueueSend(g_pingpang_queue, &state, 0);
}

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
static void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    ESP_LOGW(TAG_RHEA, "Enter %s, rssi:%d, snr=%d", __func__, rssi, snr);
    Radio.Sleep( );
    BufferSize = size;
    memcpy( Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    averageRssiValue += rssi;
    averageSnrValue += snr;
    States_t state = RX;
    xQueueSend(g_pingpang_queue, &state, 0);
}

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
static void OnTxTimeout( void )
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    Radio.Sleep( );
    States_t state = TX_TIMEOUT;
    xQueueSend(g_pingpang_queue, &state, 0);
}

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
static void OnRxTimeout( void )
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    Radio.Sleep( );
    States_t state = RX_TIMEOUT;
    xQueueSend(g_pingpang_queue, &state, 0);
}

/*!
 * \brief Function executed on Radio Rx Error event
 */
static void OnRxError( void )
{
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    Radio.Sleep( );
    States_t state = RX_ERROR;
    xQueueSend(g_pingpang_queue, &state, 0);
}

static void ping_pong_task(void *pvParameter)
{
    static bool isMaster;
    ESP_LOGW(TAG_RHEA, "Enter %s", __func__);
    uint16_t i;
    States_t State = LOWPOWER;

    isMaster = true;
    i = 0;
    while (true)
    {
        gChannels[i] = 370100000 + i * 200000;
        if (gChannels[i] > 525000000)
        {
            break;
        }
        i++;
    }
    ESP_LOGE(TAG_RHEA, "There are %d frequency channel", i);
    vTaskDelay(1004 / portTICK_PERIOD_MS);

    // Target board initialization
    BoardInitMcu( );
    BoardInitPeriph( );

    // Radio initialization
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;

    Radio.Init( &RadioEvents );

    Radio.SetChannel( gFrequency );
    Radio.SetTxConfig( modem, power, fdev, bandwidth,
                                   datarate, coderate,
                                   preambleLen, fixLen,
                                   crcOn, freqHopOn, hopPeriod, iqInverted, txTimeout);

    Radio.SetRxConfig( modem, bandwidth, datarate,
                                   coderate, bandwidthAfc, preambleLen,
                                   symbTimeout, fixLen, payloadLen,
                                   crcOn, freqHopOn, hopPeriod, iqInverted, rxContinuous);
    Radio.Rx( RX_TIMEOUT_VALUE );
    while( 1 )
    {
        switch( State )
        {
        case RX:
            if( isMaster == true )
            {
                if( BufferSize > 0 )
                {
                    rxTimeOutCount = 0;
                    dumpBytes(Buffer, BufferSize);
                    if( strncmp( ( const char* )Buffer, ( const char* )PongMsg, PING_PONG_LEN ) == 0 )
                    {
                        if (gLoraAutoTest == true)
                        {
                            gRxDoneCounter++;
                            if (gRxDoneCounter >= RX_DONE_COUNTER_MAX)
                            {
                                ESP_LOGV(TAG_RHEA, "Frequency %d test finished, average rssi:%d, average snr:%d let's test the next.", 
                                                    gFrequency, (int)(averageRssiValue/gRxDoneCounter), (int)(averageSnrValue/gRxDoneCounter));
                                LedToggle(LED_BLE, 3);
                                gIfChangeFreq = true;
                                gFrequency = gChannels[gChannelIdx];
                                if (gFrequency > 525000000 || gFrequency == 0) // Roll-back.
                                {
                                    gChannelIdx = 0;
                                    gFrequency = gChannels[gChannelIdx];
                                }
                                gRxDoneCounter = 0;
                                averageRssiValue = 0;
                                averageSnrValue = 0;

                                memset(Buffer, 0, PAY_LOAD_LENGTH);
                                sprintf((char*)Buffer, "cha%u", gChannelIdx);
                                uint8_t len = strlen((char*)Buffer);
                                for( i = len; i < PAY_LOAD_LENGTH; i++ )
                                {
                                    Buffer[i] = i - len;
                                }
                                gChannelIdx++;
                                rhea_nvs_save_idx();
                                Radio.Send( Buffer, PAY_LOAD_LENGTH );
                                
                                goto LEAVE_CASE;
                            }
                        }
                        // Indicates on a LED that the received frame is a PONG
                        //GpioWrite( &Led1, GpioRead( &Led1 ) ^ 1 );
                        LedToggle(LED_BLE, 0);

                        // Send the next PING frame
                        Buffer[0] = 'L';
                        Buffer[1] = 'P';
                        Buffer[2] = 'I';
                        Buffer[3] = 'N';
                        Buffer[4] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = PING_PONG_LEN; i < PAY_LOAD_LENGTH; i++ )
                        {
                            Buffer[i] = i - PING_PONG_LEN;
                        }
                        DelayMs( 1 );
                        Radio.Send( Buffer, PAY_LOAD_LENGTH );
                    }
                    else if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, PING_PONG_LEN ) == 0 )
                    { // A master already exists then become a slave
                    #if 0// I'm master , always master.
                        isMaster = false;
                    #endif
                        //GpioWrite( &Led2, 1 ); // Set LED off
                        LedOff(LED_BLE);
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                    else // valid reception but neither a PING or a PONG message
                    {    // Set device as master ans start again
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                }
            }
            else // slave
            {
                if( BufferSize > 0 )
                {
                    dumpBytes(Buffer, BufferSize);
                    if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, PING_PONG_LEN ) == 0 )
                    {
                        // Indicates on a LED that the received frame is a PING
                        //GpioWrite( &Led1, GpioRead( &Led1 ) ^ 1 );
                        

                        // Send the reply to the PONG string
                        Buffer[0] = 'L';
                        Buffer[1] = 'P';
                        Buffer[2] = 'O';
                        Buffer[3] = 'N';
                        Buffer[4] = 'G';
                        
                        // We fill the buffer with numbers for the payload
                        for( i = PING_PONG_LEN; i < PAY_LOAD_LENGTH; i++ )
                        {
                            Buffer[i] = i - PING_PONG_LEN;
                        }
                        //DelayMs( 1 );
                        LedToggle(LED_BLE, 2);
                        Radio.Send( Buffer, PAY_LOAD_LENGTH );
                    }
                    else if(strncmp((char*)Buffer, "cha", 3 ) == 0)
                    {
                        char *idxstr = (char*)Buffer + 3;
                        uint16_t idx = atol(idxstr);
                        gFrequency = gChannels[idx];
                        Radio.SetChannel( gFrequency );
                        DelayMs( 10 );
                        // Send the reply to the PONG string
                        Buffer[0] = 'L';
                        Buffer[1] = 'P';
                        Buffer[2] = 'O';
                        Buffer[3] = 'N';
                        Buffer[4] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = PING_PONG_LEN; i < PAY_LOAD_LENGTH; i++ )
                        {
                            Buffer[i] = i - PING_PONG_LEN;
                        }
                        Radio.Send( Buffer, PAY_LOAD_LENGTH );
                        rhea_nvs_save_frequency();
                        ESP_LOGV(TAG_RHEA, "OH, master tell me to change the frequency...Let me do it.");
                        LedToggle(LED_BLE, 3);
                    }
                    else // valid reception but not a PING as expected
                    {    // Set device as master and start again
                    #if 0
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    #else
                        ESP_LOGE(TAG_RHEA,"OH, what I have received!!!");
                        // Equal timeout logic.
                        //DelayRandomMs();
                        //Radio.Rx( RX_TIMEOUT_VALUE );
                        strcpy((char*)Buffer, "nicemeetU");
                        Radio.Send( Buffer, PAY_LOAD_LENGTH );
                    #endif
                    }
                }
            }
            LEAVE_CASE:
            State = LOWPOWER;
            break;
        case TX:
            txTimeOutCount = 0;
            // Indicates on a LED that we have sent a PING [Master]
            // Indicates on a LED that we have sent a PONG [Slave]
            //GpioWrite( &Led2, GpioRead( &Led2 ) ^ 1 );
            LedToggle(LED_BLE, 1);
            if (gIfChangeFreq)
            {
                gIfChangeFreq = false;
                esp_err_t ret = rhea_nvs_save_frequency();
                if (ret == ESP_OK)
                {
                    char *buffers = malloc(64);
                    sprintf(buffers, "Frequency change OK! -- %s -- %llu\r\n", gCID, system_get_rtc_time());
                    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char *)buffers, strlen(buffers), 0, 0);
                    free(buffers);
                }
                ESP_LOGV(TAG_RHEA, "Let us change freq.");
                Radio.SetChannel( gFrequency );
            }
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case RX_ERROR:
            //reinit_lora();
        case RX_TIMEOUT:
            //LedToggle(LED_BLE, 3);
            if (gIfChangeFreq)
            {
                gIfChangeFreq = false;
                esp_err_t ret = rhea_nvs_save_frequency();
                if (ret == ESP_OK)
                {
                    char *buffers = malloc(64);
                    sprintf(buffers, "Frequency change Sucess! -- %s -- %llu\r\n", gCID, system_get_rtc_time());
                    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char *)buffers, strlen(buffers), 0, 0);
                    free(buffers);
                }
                ESP_LOGW(TAG_RHEA, "Let us change freq.");
                Radio.SetChannel( gFrequency );
            }
            DelayRandomMs();
            if( isMaster == true )
            {
                if (gLoraAutoTest)
                {
                    rxTimeOutCount++;
                    if (rxTimeOutCount > 29) // About 180s
                    {
                        rxTimeOutCount = 0;
                        gFrequency = gChannels[gChannelIdx++];
                        char *buffers = malloc(64);
                        sprintf(buffers, "setfreq=%u", gFrequency);
                        mqtt_publish(g_mqtt_client, MQTT_SUB_TOPIC, (char *)buffers, strlen(buffers), 0, 0);
                        free(buffers);
                        rhea_nvs_save_idx();
                    }
                }
                // Send the next PING frame
                Buffer[0] = 'L';
                Buffer[1] = 'P';
                Buffer[2] = 'I';
                Buffer[3] = 'N';
                Buffer[4] = 'G';
                for ( i = PING_PONG_LEN; i < PAY_LOAD_LENGTH; i++)
                {
                    Buffer[i] = i - PING_PONG_LEN;
                }
                Radio.Send( Buffer, PAY_LOAD_LENGTH);
            }
            else
            {
                Radio.Rx( RX_TIMEOUT_VALUE );
            }
            State = LOWPOWER;
            break;
        case TX_TIMEOUT:
            if( isMaster && gLoraAutoTest)
            {
                txTimeOutCount++;
                if (txTimeOutCount > 22) // About 180s
                {
                    txTimeOutCount = 0;
                    gFrequency = gChannels[gChannelIdx++];
                    char *buffers = malloc(64);
                    sprintf(buffers, "setfreq=%u", gFrequency);
                    mqtt_publish(g_mqtt_client, MQTT_SUB_TOPIC, (char *)buffers, strlen(buffers), 0, 0);
                    free(buffers);
                    if (gLoraAutoTest)
                    {
                        rhea_nvs_save_idx();
                    }
                }
            }
            reinit_lora();
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case LOWPOWER:
        default:
            // Set low power
            break;
        }

        //TimerLowPowerHandler( );
        xQueueReceive(g_pingpang_queue, &State, portMAX_DELAY);
        if (isMaster)
        {
            ESP_LOGE(TAG_RHEA, "State = %d, I'm master", State);
        }
        else
        {
            ESP_LOGE(TAG_RHEA, "State = %d, I'm slave", State);
        }
    }
}

void lora_ping_pong_main(void)
{
    ESP_LOGW(TAG_RHEA, "Enter %s, sw version: %s, freememory:%d", __func__, DIONE_SW_VERSION, esp_get_free_heap_size());
    rhea_nvs_init();

    xTaskCreate(led_Toggle_task, "led_task", 2048, NULL, 1, NULL);
    g_pingpang_queue = xQueueCreate(10, sizeof(States_t));
    xTaskCreate(ping_pong_task, "ping_pong", 8192, NULL, 24, NULL);
    xTaskCreate(rhea_monitor_task, "rhea_monitor", 2048, NULL, 2, NULL);

    ESP_LOGW(TAG_RHEA, "Leave %s", __func__);
}

