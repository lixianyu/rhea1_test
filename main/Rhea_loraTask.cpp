/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#define RHEA_LORATASK_CPP
#include "IPAddress.h"
#include "WiFiUdp.h"
#include "Rhea_loraTask.h"
#include "loraModem.h"
#include <sys/socket.h>
#include "Dione_event.h"
//#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "esp_system.h"
#if 0
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"


#include "driver/gpio.h"
#endif
#include "driver/spi_master.h"
#include "dione_config.h"
//#include "sx1276-LoRa.h"
//#include "mqtt.h"
#include "esp_log.h"
#include "esp_bt_defs.h"

//#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
//#pragma GCC diagnostic pop

extern "C" void Dione_getTime(void *pvParameters);
extern "C" uint64_t system_get_rtc_time(void);
extern "C" void dumpBytes(const uint8_t *data, size_t count);
extern "C" void dumpBytes1(uint8_t addr,const uint8_t *data, size_t count);
extern int receivePacket();
extern void Interrupt_0(void);
extern void Interrupt_1(void);
extern void rxLoraModem();
extern void hop();
extern void cadScanner();
extern void initLoraModem();
extern void printState(uint8_t i);

static const char *TAG = "LoraGW";

#define RX_BUFF_SIZE  1024						// Downstream received from MQTT
#define STATUS_SIZE	  512						// Should(!) be enough based on the static text .. was 1024

spi_device_handle_t g_spi_handle;

char b64[256];
bool sx1272 = false;								// Actually we use sx1276/RFM95

uint32_t cp_nb_rx_rcv = 0; // The number of radio frames received since gateway start
uint32_t cp_nb_rx_ok = 0; // The number of radio frames received with correct CRC since gateway start
uint32_t cp_nb_rx_bad;
uint32_t cp_nb_rx_nocrc;
uint32_t cp_up_pkt_fwd = 0; // The number of radio frames forwarded to the gateway's network server since gateway start
uint32_t cp_pkt_ack = 0;
uint32_t g_dwnb = 0; // The number of radio frames received (from the network server) for transmission since gateway start
uint32_t g_txnb = 0; // The number of radio frames transmitted since gateway start

WiFiUDP Udp;
uint32_t stattime = 0;							// last time we sent a stat message to server
uint32_t pulltime = 0;							// last time we sent a pull_data request to server
uint32_t lastTmst = 0;
uint32_t gLastTmstRecvFromNode = 0;
IPAddress ttnServer;							// IP Address of thethingsnetwork server
uint8_t buff_down[RX_BUFF_SIZE];				// Buffer for downstream
uint16_t lastToken = 0x00;
static int mysocket;
static struct sockaddr_in remote_addr;
static unsigned int socklen;

char gGWEUIStr[20] = "d3e751dcede67688";
static uint64_t lgwm = 0; /* Lora gateway MAC address */
/* gateway <-> MAC protocol variables */
uint32_t net_mac_h; /* Most Significant Nibble, network order */
uint32_t net_mac_l; /* Least Significant Nibble, network order */

// Set spreading factor (SF7 - SF12)
sf_t sf 			= _SPREADING;
sf_t sfi 			= _SPREADING;				// Initial value of SF

// Set location, description and other configuration parameters
// Defined in ESP-sc_gway.h
//
float lat			= _LAT;						// Configuration specific info...
float lon			= _LON;
int   alt			= _ALT;
char platform[24]	= _PLATFORM; 				// platform definition
char email[40]		= _EMAIL;    				// used for contact email
char description[64] = _DESCRIPTION;				// used for free form description
#define EXAMPLE_ESP_UDP_PERF_TX

//#define LORA_SERVER_LORIOT
//#define LORA_SERVER_TOWEER
#ifdef LORA_SERVER_LORIOT
#define EXAMPLE_DEFAULT_PORT 1780
#else
#define EXAMPLE_DEFAULT_PORT 1700
#endif
//#define EXAMPLE_DEFAULT_PORT 1901

//#define EXAMPLE_DEFAULT_SERVER_IP "192.168.1.10" // Local

//#define EXAMPLE_DEFAULT_SERVER_IP "112.126.75.148" // huoweiyi

#ifdef LORA_SERVER_LORIOT
#define EXAMPLE_DEFAULT_SERVER_IP "cn1.loriot.io"
#elif defined(LORA_SERVER_TOWEER)
#define EXAMPLE_DEFAULT_SERVER_IP "47.93.102.19" // Toweer
#else
#define EXAMPLE_DEFAULT_SERVER_IP "router.cn.thethings.network"
#endif
//#define EXAMPLE_DEFAULT_SERVER_IP "52.187.168.248"

//#define EXAMPLE_DEFAULT_SERVER_IP "13.76.41.40"

#define EXAMPLE_DEFAULT_PKTSIZE 2048
#define EXAMPLE_PACK_BYTE_IS 97 //'a'

xQueueHandle g_rhea_GW_gpio_evt_queue;
static xQueueHandle g_GW_led_toggle_queue;
#if 0
// Need to save into nvs
sf_t g_lora_sf = _SPREADING;
uint8_t g_lora_ifreq = 0;
#endif

static void RheaLedToggle(uint8_t led, uint8_t time)
{
    xQueueSend(g_GW_led_toggle_queue, &time, 0);
}

static bool UDPconnect()
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    bool ret = false;
    unsigned int localPort = EXAMPLE_DEFAULT_PORT;			// To listen to return messages from WiFi
    //if (debug>=1)
    {
        ESP_LOGE(TAG, "Local UDP port %d", localPort);
    }

    if (Udp.begin(localPort) == 1)
    {
        ESP_LOGE(TAG, "Connection successful");
        ret = true;
    }
    else
    {
        ESP_LOGE(TAG, "Connection failed");
    }
    return(ret);
}
static int lora_resolve_dns(const char *host, struct sockaddr_in *ip)
{
    struct hostent *he;
    struct in_addr **addr_list;
    he = gethostbyname(host);
    if (he == NULL)
    {
        return 0;
    }
    addr_list = (struct in_addr **)he->h_addr_list;
    if (addr_list[0] == NULL)
    {
        return 0;
    }
    ip->sin_family = AF_INET;
    memcpy(&ip->sin_addr, addr_list[0], sizeof(ip->sin_addr));
    //IPAddress ipa((const uint8_t *)(he->h_addr_list[0]));
    //ttnServer = new IPAddress((const uint8_t *)(he->h_addr_list[0]));
    ttnServer = IPAddress((const uint8_t *)(he->h_addr_list[0]));
    return 1;
}

// ----------------------------------------------------------------------------
// Send UP an UDP/DGRAM message to the MQTT server
// If we send to more than one host (not sure why) then we need to set sockaddr
// before sending.
// ----------------------------------------------------------------------------
void sendUdp(uint8_t *msg, int length, bool flag = false)
{
    int l;
    lastToken = msg[2] * 256 + msg[1];

    //send the update
    Udp.beginPacket(ttnServer, (int) EXAMPLE_DEFAULT_PORT);
    if ((l = Udp.write_udp(msg, length)) != length)
    {
        ESP_LOGI(TAG, "sendUdp:: Error write");
    }
    else
    {
		if (flag == true)
		{
			cp_up_pkt_fwd++;
		}
        ESP_LOGI(TAG, "sendUdp 1: sent %d bytes", l);
    }
    //yield();
    vTaskDelay(10 / portTICK_RATE_MS);
    Udp.endPacket();

#ifdef _THINGSERVER
    delay(1);

    Udp.beginPacket(thingServer, (int) _THINGPORT);
    if ((l = Udp.write((char *)msg, length)) != length)
    {
        Serial.println("sendUdp:: Error write");
    }
    else
    {
        if (debug >= 3)
        {
            Serial.print(F("sendUdp 2: sent "));
            Serial.print(l);
            Serial.println(F(" bytes"));
        }
    }
    yield();
    Udp.endPacket();
#endif
}

// ----------------------------------------------------------------------------
// Send UP periodic status message to server even when we do not receive any
// data.
// Parameters:
//	- <none>
// ----------------------------------------------------------------------------
void sendstat(void)
{
	float up_ack_ratio;
    uint8_t status_report[STATUS_SIZE]; 					// status report as a JSON object
    char stat_timestamp[32];								// XXX was 24
    time_t now;
#if 0
    char clat[10] = {0};
    char clon[10] = {0};
#endif
    int stat_index = 0;
    uint32_t token   = esp_random(); 					// random token

    // pre-fill the data buffer with fixed fields
    status_report[0]  = PROTOCOL_VERSION;					// 0x01
    status_report[1]  = token & 0xFF;
    status_report[2]  = (token >> 8) & 0xFF;
    status_report[3]  = PKT_PUSH_DATA;						// 0x00

    *(uint32_t *)(status_report + 4) = net_mac_h;
    *(uint32_t *)(status_report + 8) = net_mac_l;

    stat_index = 12;										// 12-byte header

    // get timestamp for statistics
    time(&now);
    struct tm timeinfo;
    localtime_r(&now, &timeinfo);
    // XXX Using CET as the current timezone. Change to your timezone
    sprintf(stat_timestamp, "%04d-%02d-%02d %02d:%02d:%02d GMT-8", 1900 + timeinfo.tm_year,
            timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
    //yield();
    vTaskDelay(10 / portTICK_RATE_MS);
#if 0
    ftoa(lat, clat, 5);										// Convert lat to char array with 5 decimals
    ftoa(lon, clon, 5);										// As Arduino CANNOT prints floats
#endif
    // Build the Status message in JSON format, XXX Split this one up...
#if 0
    int j = snprintf((char *)(status_report + stat_index), STATUS_SIZE - stat_index,
                     "{\"stat\":{\"time\":\"%s\",\"lati\":%s,\"long\":%s,\"alti\":%i,\"rxnb\":%u,\"rxok\":%u,\"rxfw\":%u,\"ackr\":%u.0,\"dwnb\":%u,\"txnb\":%u,\"pfrm\":\"%s\",\"mail\":\"%s\",\"desc\":\"%s\"}}",
                     stat_timestamp, clat, clon, (int)alt, cp_nb_rx_rcv, cp_nb_rx_ok, cp_up_pkt_fwd, 0, 0, 0, platform, email, description);
#else
	if (cp_up_pkt_fwd == 0)
	{
		up_ack_ratio = 0.0;
	}
	else
	{
		up_ack_ratio = cp_pkt_ack / cp_up_pkt_fwd;
	}
    int j = snprintf((char *)(status_report + stat_index), STATUS_SIZE - stat_index,
                     "{\"stat\":{\"time\":\"%s\",\"rxnb\":%u,\"rxok\":%u,\"rxfw\":%u,\"ackr\":%.1f,\"dwnb\":%u,\"txnb\":%u,\"pfrm\":\"%s\",\"mail\":\"%s\",\"desc\":\"%s\"}}",
                     stat_timestamp, cp_nb_rx_rcv, cp_nb_rx_ok, cp_up_pkt_fwd, 100.0*up_ack_ratio, g_dwnb, g_txnb, platform, email, description);
#endif
    //yield();												// Give way to the internal housekeeping of the ESP8266
    vTaskDelay(10 / portTICK_RATE_MS);
    //if (debug >=1) { delay(1); }
    stat_index += j;
    status_report[stat_index] = 0; 							// add string terminator, for safety
#if 0
    if (debug >= 2)
    {
        Serial.print(F("stat update: <"));
        Serial.print(stat_index);
        Serial.print(F("> "));
        Serial.println((char *)(status_report + 12));			// DEBUG: display JSON stat
    }
#endif
    ESP_LOGI(TAG, "stat update: <%d> %s", stat_index, status_report + 12);

    if (stat_index > STATUS_SIZE)
    {
        ESP_LOGI(TAG, "sendstat:: ERROR buffer too big");
        return;
    }

    //send the update
    sendUdp(status_report, stat_index);
}

// ----------------------------------------------------------------------------
// Send UP periodic Pull_DATA message to server to keepalive the connection
// and to invite the server to send downstream messages when these are available
// *2, par. 5.2
//	- Protocol Version (1 byte)
//	- Random Token (2 bytes)
//	- PULL_DATA identifier (1 byte) = 0x02
//	- Gateway unique identifier (8 bytes) = MAC address
// ----------------------------------------------------------------------------
void pullData(void)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    uint8_t pullDataReq[13]; 						// status report as a JSON object
    int pullIndex = 0;
    //int i;

    // pre-fill the data buffer with fixed fields
    pullDataReq[0]  = PROTOCOL_VERSION;						// 0x01
    uint32_t token   = esp_random();
    pullDataReq[1]  = token & 0xFF;
    pullDataReq[2]  = (token >> 8) & 0xFF;
    pullDataReq[3]  = PKT_PULL_DATA;						// 0x02

    *(uint32_t *)(pullDataReq + 4) = net_mac_h;
    *(uint32_t *)(pullDataReq + 8) = net_mac_l;
    pullIndex = 12;											// 12-byte header

    pullDataReq[pullIndex] = 0; 							// add string terminator, for safety
#if 0
    if (debug >= 2)
    {
        Serial.print(F("PKT_PULL_DATA request: <"));
        Serial.print(pullIndex);
        Serial.print(F("> "));
        for (i = 0; i < pullIndex; i++)
        {
            Serial.print(pullDataReq[i], HEX);				// DEBUG: display JSON stat
            Serial.print(':');
        }
        Serial.println();
    }
#else
    dumpBytes(pullDataReq, pullIndex);
#endif
    //send the update
    sendUdp(pullDataReq, pullIndex);
}

// ----------------------------------------------------------------------------
// Read DOWN a package from UDP socket, can come from any server
// Messages are received when server responds to gateway requests from LoRa nodes
// (e.g. JOIN requests etc.) or when server has downstream data.
// We repond only to the server that sent us a message!
// Note: So normally we can forget here about codes that do upstream
// ----------------------------------------------------------------------------
static int readUdp(int packetSize, uint8_t *buff_down)
{
	ESP_LOGI(TAG, "Enter %s, packetSize=%d", __func__, packetSize);
    uint8_t protocol;
    uint16_t token;
    uint8_t ident;
    uint8_t buff[64]; 						// General buffer to use for UDP

    if (packetSize > RX_BUFF_SIZE)
    {
        ESP_LOGI(TAG, "readUDP:: ERROR package of size: %d", packetSize);
        Udp.flush();
        return(-1);
    }

    Udp.read_udp(buff_down, packetSize);
    dumpBytes(buff_down, 4);

    IPAddress remoteIpNo = Udp.remoteIP();
    unsigned int remotePortNo = Udp.remotePort();

    uint8_t *data = buff_down + 4;
	if (packetSize > 4)
	{
		ESP_LOGV(TAG, "recv UDP data= %s", data);
	}
    protocol = buff_down[0];
    token = buff_down[2] * 256 + buff_down[1];
    ident = buff_down[3];

    // now parse the message type from the server (if any)
    switch (ident)
    {
    // This message is used by the gateway to send sensor data to the
    // server. As this function is used for downstream only, this option
    // will never be selected but is included as a reference only
    case PKT_PUSH_DATA: // 0x00 UP
        ESP_LOGI(TAG, "PKT_PUSH_DATA");
#if 0
        if (debug >= 1)
        {
            Serial.print(F("PKT_PUSH_DATA:: size "));
            Serial.print(packetSize);
            Serial.print(F(" From "));
            Serial.print(remoteIpNo);
            Serial.print(F(", port "));
            Serial.print(remotePortNo);
            Serial.print(F(", data: "));
            for (int i = 0; i < packetSize; i++)
            {
                Serial.print(buff_down[i], HEX);
                Serial.print(':');
            }
            Serial.println();
        }
#endif
        break;

    // This message is sent by the server to acknowledge receipt of a
    // (sensor) message sent with the code above.
    case PKT_PUSH_ACK:	// 0x01 DOWN
#if 0
        if (debug >= 2)
        {
            Serial.print(F("PKT_PUSH_ACK:: size "));
            Serial.print(packetSize);
            Serial.print(F(" From "));
            Serial.print(remoteIpNo);
            Serial.print(F(", port "));
            Serial.print(remotePortNo);
            Serial.print(F(", token: "));
            Serial.println(token, HEX);
            Serial.println();
        }
#endif
		cp_pkt_ack++;
        ESP_LOGI(TAG, "PKT_PUSH_ACK:: size %d, From %s, port %u, token: 0x%x", packetSize, inet_ntoa(remoteIpNo), remotePortNo, token);
        //ESP_LOGI(TAG, "transfer data with %s:%u\n",inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
        break;

    case PKT_PULL_DATA:	// 0x02 UP
        ESP_LOGI(TAG, "PKT_PULL_DATA");
        break;

    // This message type is used to confirm OTAA message to the node
    // XXX This message format may also be used for other downstream communucation
    case PKT_PULL_RESP:	// 0x03 DOWN
        ESP_LOGI(TAG, "PKT_PULL_RESP");
        lastTmst = system_get_time();					// Store the tmst this package was received
        g_dwnb++;
        // Send to the LoRa Node first (timing) and then do messaging
        if (sendPacket(data, packetSize - 4) < 0)
        {
            return(-1);
        }
		RheaLedToggle(0, 2);
        // Now respond with an PKT_TX_ACK; 0x05 UP
        buff[0] = buff_down[0];
        buff[1] = buff_down[1];
        buff[2] = buff_down[2];
        //buff[3]=PKT_PULL_ACK;				// Pull request/Change of Mogyi
        buff[3] = PKT_TX_ACK;
		*(uint32_t *)(buff + 4) = net_mac_h;
	    *(uint32_t *)(buff + 8) = net_mac_l;
        buff[12] = 0;

        // Only send the PKT_PULL_ACK to the UDP socket that just sent the data!!!
        Udp.beginPacket(remoteIpNo, remotePortNo);
        if (Udp.write_udp(buff, 13) != 13)
        {
            ESP_LOGE(TAG, "PKT_TX_ACK:: Error writing Ack");
        }
        else
        {
			#if 0
            if (debug >= 1)
            {
                Serial.print(F("PKT_TX_ACK:: tmst="));
                Serial.println(micros());
            }
			#endif
			ESP_LOGE(TAG, "PKT_TX_ACK:: OK");
        }
        //yield();
        Udp.endPacket();
#if 0
        if (debug >= 1)
        {
            Serial.print(F("PKT_PULL_RESP:: size "));
            Serial.print(packetSize);
            Serial.print(F(" From "));
            Serial.print(remoteIpNo);
            Serial.print(F(", port "));
            Serial.print(remotePortNo);
            Serial.print(F(", data: "));
            data = buff_down + 4;
            data[packetSize] = 0;
            Serial.print((char *)data);
            Serial.println(F("..."));
        }
#endif
		ESP_LOGV(TAG, "PKT_PULL_RESP:: size %d, From %s, port %u, token: 0x%x", packetSize, inet_ntoa(remoteIpNo), remotePortNo, token);
        break;

    case PKT_PULL_ACK:	// 0x04 DOWN; the server sends a PULL_ACK to confirm PULL_DATA receipt
        ESP_LOGI(TAG, "PKT_PULL_ACK:: size %d, From %s, port %u, token: 0x%x", packetSize, inet_ntoa(remoteIpNo), remotePortNo, token);
#if 0
        if (debug >= 2)
        {
            Serial.print(F("PKT_PULL_ACK:: size "));
            Serial.print(packetSize);
            Serial.print(F(" From "));
            Serial.print(remoteIpNo);
            Serial.print(F(", port "));
            Serial.print(remotePortNo);
            Serial.print(F(", data: "));
            for (int i = 0; i < packetSize; i++)
            {
                Serial.print(buff_down[i], HEX);
                Serial.print(':');
            }
            Serial.println();
        }
#endif
        break;

    default:
#if 0
#if GATEWAYMGT==1
        // For simplicity, we send the first 4 bytes too
        gateway_mgt(packetSize, buff_down);
#else
#endif
        Serial.print(F(", ERROR ident not recognized: "));
        Serial.println(ident);
#endif
        ESP_LOGI(TAG, "ERROR ident not recognized: %d", ident);
        break;
    }

    // For downstream messages, fill the buff_down buffer

    return packetSize;
}

static void rhea_lora_init_interrupt(void)
{
	// init interrupt handlers, which are shared for GPIO15 / D8, 
	// we switch on HIGH interrupts
	#if 0
	if (pins.dio0 == pins.dio1) {
		attachInterrupt(pins.dio0, Interrupt, RISING);		// Share interrupts
	}
	// Or in the traditional Comresult case
	else {
		attachInterrupt(pins.dio0, Interrupt_0, RISING);	// Separate interrupts
		attachInterrupt(pins.dio1, Interrupt_1, RISING);	// Separate interrupts		
	}
	#endif
}

static void rhea_GW_led_Toggle_task(void *pvParameters)
{
	ESP_LOGV(TAG, "Enter %s", __func__);
    //uint8_t led;
    uint8_t time;
    g_GW_led_toggle_queue = xQueueCreate(10, sizeof(uint8_t));
    while (1)
    {
        if (xQueueReceive(g_GW_led_toggle_queue, &time, portMAX_DELAY))
        {
            vTaskDelay(101 / portTICK_PERIOD_MS);
            gpio_set_level(LED_BLE, LED_ON);
            if (time == 0)
            {
                vTaskDelay(50 / portTICK_PERIOD_MS);
            }
            else if (time == 1)
            {
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(70 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
            }
            else if (time == 2)
            {
                vTaskDelay(700 / portTICK_PERIOD_MS);
            }
            else if (time == 3)
            {
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(60 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_OFF);
                vTaskDelay(60 / portTICK_PERIOD_MS);
                gpio_set_level(LED_BLE, LED_ON);
                vTaskDelay(40 / portTICK_PERIOD_MS);
            }
            gpio_set_level(LED_BLE, LED_OFF);
        }        
    }
}

static void rhea_GW_monitor_task(void *pvParameters)
{
	static uint16_t resetCount = 0;
	uint8_t reg = 0x78;
	uint8_t reg1 = 0x79;
    ESP_LOGW(TAG, "Enter %s", __func__);
    vTaskDelay(10004 / portTICK_PERIOD_MS);
    while (true)
    {
		resetCount++;
        if (resetCount > 360) // Every 60 minutes, reset the SX1278.
        {
			resetCount = 0;
			ESP_LOGV(TAG, "One hour reset the SX1278...");
			_state = S_INIT;
			initLoraModem();
			_state = S_RX;
			rxLoraModem();
        }
        //reg = readRegister(REG_VERSION);
        //reg = readRegister(REG_OPMODE);
        //reg = readRegister(REG_MODEM_CONFIG1);
		//reg1 = readRegister(REG_MODEM_CONFIG2);
        //ESP_LOGD(TAG, "REG_LR_VERSION=0x%02X, freememory=%d", reg, esp_get_free_heap_size());
        //ESP_LOGV(TAG, "REG_OPMODE=0x%02X, freememory=%d", reg, esp_get_free_heap_size());
        //ESP_LOGV(TAG, "REG_MODEM_CONFIG1=0x%02X, REG_MODEM_CONFIG1=0x%02X, freememory=%d", reg, reg1, esp_get_free_heap_size());
        ESP_LOGD(TAG, "freememory=%d", esp_get_free_heap_size());
        vTaskDelay(1004 / portTICK_PERIOD_MS);
        if (reg == 0x00) // Something was wrong!!!
        {
            esp_restart();
        }
        vTaskDelay(9004 / portTICK_PERIOD_MS);
    }
}

// ----------------------------------------------------------------------------
// eventHandler is the loop() function that handles the incoming events in the
// main loop() such as packet reading etc.
// This function is there to make sure we do not spend too much time in
// interrupt mode but do reading etc in loop().
// So it is not activated by interrupts directly, but interrupts set _state so
// that the buffer can be read in the main loop.
// ----------------------------------------------------------------------------
static void rhea_lora_eventHandler_task(void *pvParameters)
{
    ESP_LOGE(TAG, "Enter %s", __func__);
    while (true)
    {
        xEventGroupWaitBits(dione_event_group, LORA_GW_STATE_RXDONE, true, false, portMAX_DELAY);

        int buff_index = 0;

        // The type of interrupt is dependent on the state we are in
        // at the moment of receiving.
        switch (_state)
        {

        // state tells us that the system was receiving a LoRa message
        // and that all data is now in the FIFO
        case S_RXDONE:
			#if 0
            if (debug >= 4)
            {
                Serial.print(F("eH:: receive on SF="));
                Serial.println(sf);
            }
			#endif
			ESP_LOGE(TAG, "eH:: receive on SF=%d...%.1fs", sf, (float)system_get_time()/1000000);

            if ((buff_index = receivePacket() ) < 0)  	// read is successful
            {
                //Serial.print(F("eH:: WARNING S_RX empty message, sf="));
                //Serial.println(sf);
                ESP_LOGE(TAG, "eH:: WARNING S_RX empty message, sf=%d", sf);
            }
			
            // Reset all RX lora stuff
            _state = S_RX;
			#if 1
            rxLoraModem();
			#endif
            // If we now switch to S_SCAN, we have to hop too
            if (_hop)
            {
                hop();
            }

            if (_cad)
            {
                // Set the state to CAD scanning after receiving
                _state = S_SCAN;						// Inititialise scanner
                cadScanner();
            }
			RheaLedToggle(0, 0);
            break;

        default:
			#if 0
            if (debug >= 2)
            {
                Serial.print(F("eH:: Unrecognized state, "));
                printState(2);
                return;
            }
			#endif
			ESP_LOGE(TAG, "eH:: Unrecognized state, ");
			//printState(2);
            break;
        }//switch
    }
}

static void rhea_lora_hop_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    while (true)
    {
        vTaskDelay(portMAX_DELAY / portTICK_RATE_MS);
#if 0 // TODO:
        // The next section is emergency only. If posible we hop() in the state machine.
        // If hopping is enabled, and by lack of timer, we hop()
        // XXX Experimental, 2.5 ms between hops max
        //
        nowTime = micros();
        if ((_hop) && (((long)(nowTime - hopTime)) > 2500))
        {
            if ((_state == S_SCAN) && (sf == SF12))
            {
                if (debug >= 1) Serial.println(F("loop:: hop"));
                hop();
            }
            // XXX section below does not work wthout further work. It is the section with the MOST
            // influence on the HOP mode of operation (which is somewhat unexpected)
            // If we keep staying in another state, reset
            else if (((long)(nowTime - hopTime)) > 100000)
            {
                if (debug >= 2)
                {
                    Serial.print(F("loop:: reset, STATE="));
                    Serial.print(_state);
                    Serial.print(F(", F="));
                    Serial.print(ifreq);
                }
                _state = S_SCAN;
                hop();
                if (debug >= 2)
                {
                    Serial.print(F("->"));
                    Serial.println(ifreq);
                }
                //	rxLoraModem();
                if (_cad)
                {
                    _state = S_SCAN;
                    cadScanner();
                }
            }
            else if (debug >= 3)
            {
                Serial.print(F(" state="));
                Serial.println(_state);
            }
            inHop = false;							// Reset re-entrane protection of HOP
            yield();
        }
#endif
    }
}

static void rhea_lora_readUDP_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    int packetSize;
    uint32_t level = 0;
    while (true)
    {
        if (level % 11 == 0)
        {
            //gpio_set_level(LED_UWB, LED_ON);
        }
        // Receive UDP PUSH_ACK messages from server. (*2, par. 3.3)
        // This is important since the TTN broker will return confirmation
        // messages on UDP for every message sent by the gateway. So we have to consume them..
        // As we do not know when the server will respond, we test in every loop.
        //
        //
        while( (packetSize = Udp.parsePacket()) > 0)  		// Length of UDP message waiting
        {
            //yield();
            //vTaskDelay(10 / portTICK_RATE_MS);
            // Packet may be PKT_PUSH_ACK (0x01), PKT_PULL_ACK (0x03) or PKT_PULL_RESP (0x04)
            // This command is found in byte 4 (buff_down[3])
            if (readUdp(packetSize, buff_down) < 0)
            {
                ESP_LOGI(TAG, "readUDP error");
            }
        }
        vTaskDelay(100 / portTICK_RATE_MS);
        if (level % 11 == 0)
        {
            //gpio_set_level(LED_UWB, LED_OFF);
        }
        level++;
    }
}

static void rhea_lora_sendstat_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    vTaskDelay(5000 / portTICK_RATE_MS);
    while (true)
    {
        sendstat();
        vTaskDelay(_STAT_INTERVAL / portTICK_RATE_MS);
    }
}

static void rhea_lora_pullData_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    vTaskDelay(3000 / portTICK_RATE_MS);
    while (true)
    {
        pullData();
        vTaskDelay(_PULL_INTERVAL / portTICK_RATE_MS);
    }
}

static void rhea_lora_NTP_task(void *pvParameters)
{
    static uint8_t param = 1;
    ESP_LOGI(TAG, "Enter %s", __func__);
    while (true)
    {
        vTaskDelay(_NTP_INTERVAL / portTICK_RATE_MS);
        xTaskCreate(Dione_getTime, "getTime", 4096, &param, 1, NULL);
    }
}

static void rhea_test_task(void *pvParameters)
{
    while (1)
    {
        uint64_t now = system_get_rtc_time();
        ESP_LOGI(TAG, "rtc:%llu", now);
        vTaskDelay(1004 / portTICK_RATE_MS);
    }
}

static uint8_t m_tx_data_spi[128]; ///< SPI master TX buffer.
static uint8_t m_rx_data_spi[128]; ///< SPI master RX buffer.
void rhea_SPI_burst_read( uint8_t addr, uint8_t *buffer, uint8_t size )
{
	ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;

	memset(m_tx_data_spi, 0, sizeof(m_tx_data_spi));
	memset(m_rx_data_spi, 0, sizeof(m_rx_data_spi));
    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 8 * (1 + size);                     //adr is 8 bits
    tt.rx_buffer = m_rx_data_spi;
	m_tx_data_spi[0] = addr;
	tt.tx_buffer = m_tx_data_spi;
    ret = spi_device_transmit(g_spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "rhea_SPI_burst_read OK.");
		memcpy(buffer, m_rx_data_spi + 1, size);
    }
	else
	{
		ESP_LOGE(TAG, "rhea_SPI_burst_read fail.");
	}
	#if 0
    ret_trans = &tt;
	
	ESP_LOGI(TAG, "spi_device_get_trans_result OK.");
	ESP_LOGI(TAG, "ret_trans::flags=0x%x, command=0x%x, address=%llu, length=%d, rxlength=%d, rx_buffer=0x%x, rx_data[0]=%d,rx_data[1]=%d,rx_data[2]=%d,rx_data[3]=%d",
				ret_trans->flags, ret_trans->command, ret_trans->address, ret_trans->length,
				ret_trans->rxlength, (unsigned int)ret_trans->rx_buffer, ret_trans->rx_data[0],
				ret_trans->rx_data[1],ret_trans->rx_data[2],ret_trans->rx_data[3]);
	if (ret_trans->rx_buffer != NULL)
	{
		dumpBytes1(addr, (uint8_t*)ret_trans->rx_buffer, size);
	}
	#endif
}

// Read one byte
int8_t rhea_SPI_Read(uint8_t adr)
{
	ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;
	struct spi_transaction_t *ret_trans;

	uint8_t s = sizeof(spi_transaction_t);
	ESP_LOGD(TAG, "spi_transaction_t size is %d, size_t size = %d.", s, sizeof(size_t));
    memset(&tt, 0, s);
    tt.length = 16;                     //adr is 8 bits
    m_tx_data_spi[0] = adr;
    tt.tx_buffer = m_tx_data_spi;
	tt.rx_buffer = m_rx_data_spi;
    ret = spi_device_transmit(g_spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		ESP_LOGV(TAG, "spi_device_transmit OK.");
		return m_rx_data_spi[1];
    }
	else
	{
		ESP_LOGE(TAG, "spi_device_transmit fail.");
		return -1;
	}
#if 0
    ret_trans = &tt;
	if (1)
    {
		ESP_LOGI(TAG, "spi_device_get_trans_result OK.");
		ESP_LOGI(TAG, "ret_trans::flags=0x%x, command=0x%x, address=%llu, length=%d, rxlength=%d, rx_buffer=%p, rx_data[0]=%d,rx_data[1]=%d,rx_data[2]=%d,rx_data[3]=%d",
					ret_trans->flags, ret_trans->command, ret_trans->address, ret_trans->length,
					ret_trans->rxlength, ret_trans->rx_buffer, ret_trans->rx_data[0],
					ret_trans->rx_data[1],ret_trans->rx_data[2],ret_trans->rx_data[3]);
		dumpBytes((uint8_t*)ret_trans, s);
		if (ret_trans->rx_buffer != NULL)
		{
			dumpBytes((uint8_t*)ret_trans->rx_buffer, 16);
			return buf[1];
		}
		return ret_trans->rx_data[0];
    }
	else
	{
		ESP_LOGE(TAG, "spi_device_get_trans_result fail.");
		return -1;
	}
#endif
}

// write one byte
void rhea_SPI_Write(uint8_t adr, uint8_t data)
{
	ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;
	//struct spi_transaction_t *ret_trans;

    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 16;                     //adr is 8 bits
    m_tx_data_spi[0] = adr | 0x80;
	m_tx_data_spi[1] = data;
    tt.tx_buffer = m_tx_data_spi;
	tt.rx_buffer = m_rx_data_spi;
    ret = spi_device_transmit(g_spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		ESP_LOGW(TAG, "rhea_SPI_Write OK.");
		#if 0
		ret_trans = &tt;
		ESP_LOGW(TAG, "ret_trans::flags=0x%x, command=0x%x, address=%llu, length=%d, rxlength=%d, rx_buffer=%p, rx_data[0]=%d,rx_data[1]=%d,rx_data[2]=%d,rx_data[3]=%d",
					ret_trans->flags, ret_trans->command, ret_trans->address, ret_trans->length,
					ret_trans->rxlength, ret_trans->rx_buffer, ret_trans->rx_data[0],
					ret_trans->rx_data[1],ret_trans->rx_data[2],ret_trans->rx_data[3]);
		dumpBytes((uint8_t*)ret_trans, sizeof(spi_transaction_t));
		return ret_trans->rx_data[0];
		#endif
    }
	else
	{
		ESP_LOGE(TAG, "rhea_SPI_Write fail!");
	}
}

void rhea_SPI_burst_Write(uint8_t adr, uint8_t *buffer, uint8_t size)
{
	ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;
	struct spi_transaction_t *ret_trans;

    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 8 * (1 + size);                     //adr is 8 bits
    m_tx_data_spi[0] = adr | 0x80;
	memcpy(m_tx_data_spi + 1, buffer, size);
    tt.tx_buffer = m_tx_data_spi;
	tt.rx_buffer = m_rx_data_spi;
    ret = spi_device_transmit(g_spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		ESP_LOGW(TAG, "rhea_SPI_burst_Write Sucess.");
		#if 0
		ret_trans = &tt;
		ESP_LOGW(TAG, "ret_trans::flags=0x%x, command=0x%x, address=%llu, length=%d, rxlength=%d, rx_buffer=%p, rx_data[0]=%d,rx_data[1]=%d,rx_data[2]=%d,rx_data[3]=%d",
					ret_trans->flags, ret_trans->command, ret_trans->address, ret_trans->length,
					ret_trans->rxlength, ret_trans->rx_buffer, ret_trans->rx_data[0],
					ret_trans->rx_data[1],ret_trans->rx_data[2],ret_trans->rx_data[3]);
		dumpBytes((uint8_t*)ret_trans, sizeof(spi_transaction_t));
		return ret_trans->rx_data[0];
		#endif
    }
	else
	{
		ESP_LOGE(TAG, "rhea_SPI_burst_Write fail!");
	}
}
#if 0
static void SX1276ReadBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
	//ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;

	memset(m_tx_data_spi, 0, sizeof(m_tx_data_spi));
	memset(m_rx_data_spi, 0, sizeof(m_rx_data_spi));
    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 8 * (1 + size);                     //adr is 8 bits
    tt.rx_buffer = m_rx_data_spi;
	m_tx_data_spi[0] = addr;
	tt.tx_buffer = m_tx_data_spi;
    ret = spi_device_transmit(g_spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		//ESP_LOGI(TAG, "rhea_SPI_burst_read OK.");
		memcpy(buffer, m_rx_data_spi + 1, size);
    }
	else
	{
		ESP_LOGE(TAG, "rhea_SPI_burst_read fail.");
	}
}
static void SX1276Read( uint8_t addr, uint8_t *data )
{
    SX1276ReadBuffer( addr, data, 1 );
}
#endif
static void rhea_SPI_test_task(void *pvParameters)
{
	ESP_LOGI(TAG, "Enter %s", __func__);
	xEventGroupWaitBits(dione_event_group, DIONE_EVENT_STA_GOT_IP, false, false, portMAX_DELAY);

	esp_err_t ret;
	spi_bus_config_t buscfg;
	spi_device_interface_config_t devcfg;
	memset(&buscfg, 0, sizeof(buscfg));
	memset(&devcfg, 0, sizeof(devcfg));
	buscfg.miso_io_num = RHEA_LORA_MISO;
	buscfg.mosi_io_num = RHEA_LORA_MOSI;
	buscfg.sclk_io_num = RHEA_LORA_CLK;
	buscfg.quadwp_io_num = -1;
	buscfg.quadhd_io_num = -1;

	devcfg.clock_speed_hz = 1000000;
	devcfg.command_bits = 0;
	devcfg.mode = 0;
	devcfg.spics_io_num = RHEA_LORA_SS;
	devcfg.queue_size = 8;
	#if 0
    spi_device_interface_config_t devcfg={
        .clock_speed_hz=10000000,               //Clock out at 10 MHz
        .mode=0,                                //SPI mode 0
        .spics_io_num=RHEA_LORA_SS,               //CS pin
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
        .pre_cb=ili_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
    };
	#endif
    // Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_initialize sucess.");
    }
	else
	{
		ESP_LOGE(TAG, "spi_bus_initialize fail.");
	}
    // Attach the SX1278/1276 to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &g_spi_handle);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_add_device sucess.");
    }
	else
	{
		ESP_LOGI(TAG, "spi_bus_add_device fail.");
	}
	uint8_t nihao[64] = {0};
	bool flag = true;
    while (1)
    {
		#if 0
		gpio_set_level(RHEA_LORA_RESET, 0);
		vTaskDelay(1 / portTICK_RATE_MS);
		gpio_set_level(RHEA_LORA_RESET, 1);
        vTaskDelay(10 / portTICK_RATE_MS);
		#endif
		#if 0
		uint8_t reg = rhea_SPI_Read(REG_OPMODE);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_OPMODE, reg);
		//vTaskDelay(10 / portTICK_RATE_MS);
		reg = rhea_SPI_Read(REG_VERSION);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_VERSION, reg);
		vTaskDelay(6210 / portTICK_RATE_MS);
		#endif
		#if 0
		//uint8_t he[128] = {0};
		uint8_t he;
		rhea_SPI_burst_read(REG_OPMODE, &he, 1);
		dumpBytes1(REG_OPMODE, &he, 0x1);
		vTaskDelay(10010 / portTICK_RATE_MS);
		#endif
		#if 0
		uint8_t reg = rhea_SPI_Read(REG_OPMODE);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_OPMODE, reg);
		//vTaskDelay(10 / portTICK_RATE_MS);
		rhea_SPI_Write(REG_OPMODE, 0x08);
		rhea_SPI_Write(REG_OPMODE, 0x88);
		//vTaskDelay(10 / portTICK_RATE_MS);
		reg = rhea_SPI_Read(REG_OPMODE);
		ESP_LOGE(TAG, "reg[%X] = 0x%X", REG_OPMODE, reg);
		vTaskDelay(7210 / portTICK_RATE_MS);
		#endif
		#if 0
		if (flag)
		{
			rhea_SPI_Write(REG_OPMODE, 0x08); // Into sleep mode.
			//vTaskDelay(100 / portTICK_RATE_MS);
			//rhea_SPI_Write(REG_OPMODE, 0x89);
			//vTaskDelay(100 / portTICK_RATE_MS);
		}
		uint8_t he[32] = {0};
		//rhea_SPI_burst_read(REG_LR_FRFMSB, he, 3);
		uint8_t reg = rhea_SPI_Read(REG_OPMODE);
		ESP_LOGE(TAG, "reg[%X] = 0x%02X", REG_OPMODE, reg);
		//vTaskDelay(100 / portTICK_RATE_MS);
		he[0] = 0x6D;
		he[1] = 0x82;
		he[2] = 0xA2;
		//rhea_SPI_burst_Write(REG_LR_FRFMSB, he, 3);
		//rhea_SPI_Write(REG_LR_FRFMSB, 0x6d);
		if (flag) 
		{
			flag = false;
			#if 0
			rhea_SPI_Write(REG_LR_FRFMSB, 0x6d);
			//vTaskDelay(100 / portTICK_RATE_MS);
			rhea_SPI_Write(REG_LR_FRFMID, 0x7f);
			//vTaskDelay(100 / portTICK_RATE_MS);
			rhea_SPI_Write(REG_LR_FRFLSB, 0x03);
			#else
			rhea_SPI_burst_Write(REG_LR_FRFMSB, he, 3);
			#endif
		}
		//rhea_SPI_Write(REG_LR_FRFLSB, he[2]);
		//vTaskDelay(5000 / portTICK_RATE_MS);
		#if 1
		rhea_SPI_burst_read(REG_LR_FRFMSB, nihao, 3);
		dumpBytes1(REG_LR_FRFMSB, nihao, 24);
		#else
		reg = rhea_SPI_Read(REG_LR_FRFMSB);
		ESP_LOGE(TAG, "reg[%X] = 0x%02X", REG_LR_FRFMSB, reg);
		reg = rhea_SPI_Read(REG_LR_FRFMID);
		ESP_LOGE(TAG, "reg[%X] = 0x%02X", REG_LR_FRFMID, reg);
		reg = rhea_SPI_Read(REG_LR_FRFLSB);
		ESP_LOGE(TAG, "reg[%X] = 0x%02X", REG_LR_FRFLSB, reg);
		#endif
		ESP_LOGI(TAG, "..........................................................................\r\n");
		vTaskDelay(10010 / portTICK_RATE_MS);
		#endif
		#if 0
		uint8_t reg = 0x76;
		SX1276Read(REG_LR_OPMODE, &reg);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_LR_OPMODE, reg);
		vTaskDelay(6010 / portTICK_RATE_MS);
		#endif
    }
}

static void IRAM_ATTR rhea_GW_gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    //gpio_isr_handler_remove(DIONE_SHAKE_PIN);
    xQueueSendFromISR(g_rhea_GW_gpio_evt_queue, &gpio_num, NULL);
}

static void rhea_GW_Irq_task(void *pvParameters)
{
    ESP_LOGW(TAG, "Enter %s", __func__);
    uint32_t io_num;
    while (1)
    {
        if (xQueueReceive(g_rhea_GW_gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            ESP_LOGV(TAG, "io_num = %u", io_num);
            switch (io_num)
            {
                case RHEA_LORA_DIO0:
                    Interrupt_0();
                    break;
                case RHEA_LORA_DIO1:
                    Interrupt_1();
                    break;
                case RHEA_LORA_DIO2:
                    break;
                case RHEA_LORA_DIO3:
                    break;
                default:
                    break;
            }
        }
    }
}

static void Rhea_GW_IO_IrqInit(void)
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    gpio_config_t io_conf;
    //interrupt of rising edge
    io_conf.intr_type = GPIO_INTR_POSEDGE;
    //bit mask of the pin
    io_conf.pin_bit_mask = (1ULL << RHEA_LORA_DIO0);
    //io_conf.pin_bit_mask = DIO0_INPUT_PIN_SEL;
    //set as input mode    
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    gpio_config(&io_conf);
    //create a queue to handle gpio event from isr
    g_rhea_GW_gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    //start gpio task
    xTaskCreate(rhea_GW_Irq_task, "GWIrqTask", 4096, NULL, 23, NULL);

    //install gpio isr service
    gpio_install_isr_service(0);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(RHEA_LORA_DIO0, rhea_GW_gpio_isr_handler, (void*) RHEA_LORA_DIO0);
    gpio_isr_handler_add(RHEA_LORA_DIO1, rhea_GW_gpio_isr_handler, (void*) RHEA_LORA_DIO1);
    gpio_isr_handler_add(RHEA_LORA_DIO2, rhea_GW_gpio_isr_handler, (void*) RHEA_LORA_DIO2);
    gpio_isr_handler_add(RHEA_LORA_DIO3, rhea_GW_gpio_isr_handler, (void*) RHEA_LORA_DIO3);
}

static void Rhea_GW_BoardInitPeriph( void )
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    gpio_pad_select_gpio(LED_BLE);
    gpio_set_direction(LED_BLE, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_BLE, LED_OFF);
}

static void Rhea_GW_BoardInitMcu( void )
{
    ESP_LOGI(TAG, "Enter %s", __func__);
    // SPI init.
    esp_err_t ret;
	spi_bus_config_t buscfg;
	spi_device_interface_config_t devcfg;
	memset(&buscfg, 0, sizeof(buscfg));
	memset(&devcfg, 0, sizeof(devcfg));
	buscfg.miso_io_num = RHEA_LORA_MISO;
	buscfg.mosi_io_num = RHEA_LORA_MOSI;
	buscfg.sclk_io_num = RHEA_LORA_CLK;
	buscfg.quadwp_io_num = -1;
	buscfg.quadhd_io_num = -1;

	devcfg.clock_speed_hz = 8000000;
	devcfg.command_bits = 0;
	devcfg.mode = 0;
	devcfg.spics_io_num = RHEA_LORA_SS;
	devcfg.queue_size = 8;
    // Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_initialize sucess.");
    }
	else
	{
		ESP_LOGE(TAG, "spi_bus_initialize fail.");
	}
    // Attach the SX1278/1276 to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &g_spi_handle);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_add_device sucess.");
    }
	else
	{
		ESP_LOGI(TAG, "spi_bus_add_device fail.");
	}
    
    gpio_pad_select_gpio(RHEA_LORA_RESET);
    gpio_set_direction(RHEA_LORA_RESET, GPIO_MODE_OUTPUT);
    gpio_set_level(RHEA_LORA_RESET, 1);
    #if 0
    gpio_pad_select_gpio(RHEA_LORA_DIO0);
    gpio_set_direction(RHEA_LORA_DIO0, GPIO_MODE_INPUT);
    gpio_pad_select_gpio(RHEA_LORA_DIO1);
    gpio_set_direction(RHEA_LORA_DIO1, GPIO_MODE_INPUT);
    gpio_pad_select_gpio(RHEA_LORA_DIO2);
    gpio_set_direction(RHEA_LORA_DIO2, GPIO_MODE_INPUT);
    gpio_pad_select_gpio(RHEA_LORA_DIO3);
    gpio_set_direction(RHEA_LORA_DIO3, GPIO_MODE_INPUT);
	#endif
	Rhea_GW_IO_IrqInit();
}

#ifdef LORA_SERVER_LORIOT
static void rhea_set_EUI(void)
{//30:ae:a4:03:92:d8
	uint8_t macSTA[6];
	esp_read_mac(macSTA, ESP_MAC_WIFI_STA);

	ESP_LOGW(TAG, "STA ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macSTA));
	#if 0
	lgwm = (uint64_t)(macSTA[0]<<56 | macSTA[1]<<48 | macSTA[2]<<40 | 0xFF<<32 |
		   0xFF<<24 | macSTA[3]<<16 | macSTA[4]<<8 | macSTA[5]);
	#else
	memset(gGWEUIStr, 0, sizeof(gGWEUIStr));
	sprintf(gGWEUIStr, "%02x%02x%02xffff%02x%02x%02x", ESP_BD_ADDR_HEX(macSTA));
	sscanf(gGWEUIStr, "%llx", &lgwm);
	#endif
	ESP_LOGV(TAG, "gGWEUIStr = %s", gGWEUIStr);
	ESP_LOGW(TAG, "INFO: gateway MAC address is configured to %016llX\n", lgwm);
	net_mac_h = htonl((uint32_t)(0xFFFFFFFF & (lgwm >> 32)));
    net_mac_l = htonl((uint32_t)(0xFFFFFFFF &  lgwm  ));
}
#else
static void rhea_set_EUI(void)
{
	sscanf(gGWEUIStr, "%llx", &lgwm);
    ESP_LOGI(TAG, "INFO: gateway MAC address is configured to %016llX\n", lgwm);
    net_mac_h = htonl((uint32_t)(0xFFFFFFFF & (lgwm >> 32)));
    net_mac_l = htonl((uint32_t)(0xFFFFFFFF &  lgwm  ));
}
#endif

static void rhea_lora_setup_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Enter %s", __func__);

	vTaskDelay(3000 / portTICK_RATE_MS);
	Rhea_GW_BoardInitMcu();
	Rhea_GW_BoardInitPeriph();

	rhea_set_EUI();
	/*wating for connecting to AP and got IP.*/
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_STA_GOT_IP, false, false, portMAX_DELAY);
    /*Let us use DNS to get server IP once */
    while (1)
    {
        bzero(&remote_addr, sizeof(remote_addr));
        remote_addr.sin_family = AF_INET;
        //sock_info.sin_port = htons(atoi(EXAMPLE_SERVER_PORT));
        remote_addr.sin_port = htons(EXAMPLE_DEFAULT_PORT);

        //if host is not ip address, resolve it
        if (inet_aton( EXAMPLE_DEFAULT_SERVER_IP, &(remote_addr.sin_addr)) == 0)
        {
            ESP_LOGI(TAG, "Resolve dns for domain: %s", EXAMPLE_DEFAULT_SERVER_IP);
            if (!lora_resolve_dns(EXAMPLE_DEFAULT_SERVER_IP, &remote_addr))
            {
                vTaskDelay(2000 / portTICK_RATE_MS);
                ESP_LOGE(TAG, "Resolve dns fail, try again...");
                continue;
            }
            else
            {
                ESP_LOGI(TAG, "Resolve dns OK: %s:%u\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
                break;
            }
        }
        else
        {
            ttnServer = IPAddress(remote_addr.sin_addr.s_addr);
            ESP_LOGI(TAG, "OH, It is already ip address");
            break;
        }
    }


    // If we are here we are connected to WLAN and got IP,
    // So now test the UDP function
    if (!UDPconnect())
    {
        ESP_LOGE(TAG, "Error UDPconnect");
    }
    vTaskDelay(500 / portTICK_RATE_MS);
	// Setup ad initialise LoRa state machine of _loramModem.ino
	_state = S_INIT;
	initLoraModem();
	_state = S_RX;
	rxLoraModem();
	if (_cad) {
		_state = S_SCAN;
		cadScanner();										// Always start at SF7
	}
	xTaskCreate(rhea_GW_led_Toggle_task, "led_task", 2048, NULL, 4, NULL);
    xTaskCreate(rhea_lora_eventHandler_task, "lora_EH", 5120, NULL, 24, NULL);
    xTaskCreate(rhea_lora_hop_task, "lora_hop", 2048, NULL, 9, NULL);
    xTaskCreate(rhea_lora_readUDP_task, "lora_readUDP", 5120, NULL, 10, NULL);
    xTaskCreate(rhea_lora_sendstat_task, "lora_stat", 5120, NULL, 7, NULL);
    xTaskCreate(rhea_lora_pullData_task, "lora_pull", 5120, NULL, 6, NULL);
    xTaskCreate(rhea_lora_NTP_task, "lora_ntp", 2048, NULL, 5, NULL);
    //xTaskCreate(rhea_test_task, "lora_test", 2048, NULL, 4, NULL);
    xTaskCreate(rhea_GW_monitor_task, "GW_monitor", 2048, NULL, 3, NULL);

	//gpio_set_level(RHEA_LORA_RESET, 1);
    //xTaskCreate(rhea_SPI_test_task, "lora_SPI_test", 3072, NULL, 18, NULL);
    
    vTaskDelete(NULL);
}

extern "C" void Rhea_lora_gateway_init(void)
{
    ESP_LOGE(TAG, "Enter %s", __func__);

    //UDPconnect();
    //xTaskCreate(udp_conn_test, "lora", 5120, NULL, 9, NULL);
    xTaskCreate(rhea_lora_setup_task, "lora_setup", 5120, NULL, 10, NULL);
}

